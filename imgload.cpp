#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "png.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	bool ispec = false;
	std::string imgfile;
	uint32_t fps_n = 1;
	uint32_t fps_d = 2;
	char interlace = 'p';
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--duration=([1-9][0-9]*)", arg)) {
			try {
				fps_n = 1;
				fps_d = parse_value<unsigned>(r[1]);
			} catch(std::exception& e) {
				std::cerr << "imgload: Bad duration '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--duration=([1-9][0-9]*)/([1-9][0-9]*)", arg)) {
			try {
				fps_n = parse_value<unsigned>(r[2]);
				fps_d = parse_value<unsigned>(r[1]);
			} catch(std::exception& e) {
				std::cerr << "imgload: Bad duration '" << r[1] << "/" << r[2] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--interlace=top", arg)) {
			interlace = 't';
		} else if(r = regex("--interlace=bottom", arg)) {
			interlace = 'b';
		} else if(r = regex("--.*", arg)) {
			std::cerr << "imgload: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		} else {
			if(ispec) {
				std::cerr << "imgload: Only one image can be specified" << std::endl;
				return 1;
			}
			ispec = true;
			imgfile = arg;
		}
	}
	if(!ispec) {
		std::cerr << "imgload: Image to load required" << std::endl;
		return 1;
	}

	//Open files.
	FILE* in = fopen(imgfile.c_str(), "rb");
	if(!in) {
		std::cerr << "imgload: Can't open '" << imgfile << "'" << std::endl;
		return 1;
	}
	FILE* out = stdout;
	mark_pipe_as_binary(out);
	
	try {
		regex_results r;
		yuv4mpeg_stream_header phdr;

		parsed_png image(imgfile);

		phdr.width = image.width;
		phdr.height = image.height;
		phdr.chroma = "rgb";
		phdr.interlace = interlace;
		phdr.fps_n = fps_n;
		phdr.fps_d = fps_d;
		write_or_die(out, static_cast<std::string>(phdr));

		yuv4mpeg_frame_header framh;
		std::vector<char> buffer;
		buffer.resize(3 * phdr.width * phdr.height);

		for(size_t i = 0; i < phdr.width * phdr.height; i++) {
			buffer[3 * i + 0] = image.data[i];
			buffer[3 * i + 1] = image.data[i] >> 8;
			buffer[3 * i + 2] = image.data[i] >> 16;
		}

		write_or_die(out, std::string(framh));
		write_or_die(out, &buffer[0], buffer.size());

		
	} catch(std::exception& e) {
		std::cerr << "imgload: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
