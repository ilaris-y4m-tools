#include "yuvconvert.hpp"
#include <cstdlib>
#include <stdexcept>
extern "C"
{
#ifndef UINT64_C
#define UINT64_C(val) val##ULL
#endif
#include <libswscale/swscale.h>
}

namespace
{
	PixelFormat sws_fmt;

	void init_tables()
	{
		uint16_t magic = 258;
		sws_fmt = (*reinterpret_cast<uint8_t*>(&magic) == 2) ? PIX_FMT_GRAY16LE : PIX_FMT_GRAY16BE;
	}

	template<typename outv, typename inv>
	void reduce_luma(outv* out, const inv* in, size_t width, size_t height)
	{
		if(sizeof(outv) == sizeof(inv)) {
			memcpy(out, in, sizeof(inv) * width * height);
			return;
		}
		for(size_t i = 0; i < width * height; i++) {
			if(sizeof(outv) == 2 && sizeof(inv) == 1)
				out[i] = ((outv)in[i] << 8) | in[i];
			if(sizeof(outv) == 1 && sizeof(inv) == 2)
				out[i] = in[i] >> 8;
		}
	}

	template<typename outv, typename inv>
	void reduce_chroma_gauss(outv* out, const inv* in, size_t width, size_t height)
	{
		static SwsContext* ctx = NULL;
		PixelFormat infmt = (sizeof(inv) == 2) ? sws_fmt : PIX_FMT_GRAY8;
		PixelFormat outfmt = (sizeof(outv) == 2) ? sws_fmt : PIX_FMT_GRAY8;
		ctx = sws_getCachedContext(ctx, width, height, infmt, width / 2, height / 2, outfmt,
			SWS_GAUSS, NULL, NULL, NULL);
		const uint8_t* in_ptr[1];
		int in_stride[1];
		uint8_t* out_ptr[1];
		int out_stride[1];
		in_stride[0] = sizeof(inv) * width;
		out_stride[0] = sizeof(outv) * width / 2;
		in_ptr[0] = reinterpret_cast<const uint8_t*>(in);
		out_ptr[0] = reinterpret_cast<uint8_t*>(out);
		sws_scale(ctx, in_ptr, in_stride, 0, height, out_ptr, out_stride);
	}

	template<typename outv, typename inv>
	void reduce_chroma_bilinear(outv* out, const inv* in, size_t width, size_t height);

	template<>
	void reduce_chroma_bilinear(uint8_t* out, const uint8_t* in, size_t width, size_t height)
	{
		const uint16_t* _in = reinterpret_cast<const uint16_t*>(in);
		size_t hwidth = width / 2;
		for(size_t i = 0; i < height; i += 2) {
			for(size_t j = 0; j < hwidth; j++) {
				uint16_t v1, v2;
				v1 = _in[j];
				v2 = _in[j + hwidth];
				v1 = (v1 >> 8) + (v1 & 0xFF);
				v2 = (v2 >> 8) + (v2 & 0xFF);
				out[j] = (v1 + v2 + 2) >> 2;
			}
			_in += width;
			out += hwidth;
		}
	}

	template<>
	void reduce_chroma_bilinear(uint8_t* out, const uint16_t* in, size_t width, size_t height)
	{
		const uint32_t* _in = reinterpret_cast<const uint32_t*>(in);
		size_t hwidth = width / 2;
		for(size_t i = 0; i < height; i += 2) {
			for(size_t j = 0; j < hwidth; j++) {
				uint32_t v1, v2;
				v1 = _in[j];
				v2 = _in[j + hwidth];
				v1 = (v1 >> 16) + (v1 & 0xFFFF);
				v2 = (v2 >> 16) + (v2 & 0xFFFF);
				out[j] = (v1 + v2 + 512) >> 10;
			}
			_in += width;
			out += hwidth;
		}
	}

	template<>
	void reduce_chroma_bilinear(uint16_t* out, const uint8_t* in, size_t width, size_t height)
	{
		const uint16_t* _in = reinterpret_cast<const uint16_t*>(in);
		size_t hwidth = width / 2;
		for(size_t i = 0; i < height; i += 2) {
			for(size_t j = 0; j < hwidth; j++) {
				uint16_t v1, v2;
				v1 = _in[j];
				v2 = _in[j + hwidth];
				v1 = (v1 >> 8) + (v1 & 0xFF);
				v2 = (v2 >> 8) + (v2 & 0xFF);
				out[j] = ((v1 + v2) << 6) + ((v1 + v2 + 2) >> 2);
			}
			_in += width;
			out += hwidth;
		}
	}

	template<>
	void reduce_chroma_bilinear(uint16_t* out, const uint16_t* in, size_t width, size_t height)
	{
		const uint32_t* _in = reinterpret_cast<const uint32_t*>(in);
		size_t hwidth = width / 2;
		for(size_t i = 0; i < height; i += 2) {
			for(size_t j = 0; j < hwidth; j++) {
				uint32_t v1, v2;
				v1 = _in[j];
				v2 = _in[j + hwidth];
				v1 = (v1 >> 16) + (v1 & 0xFFFF);
				v2 = (v2 >> 16) + (v2 & 0xFFFF);
				out[j] = (v1 + v2 + 2) >> 2;
			}
			_in += width;
			out += hwidth;
		}
	}

	template<long long b, long long r, long long c, long long S, long long Sc, long long Sl, long long Bl,
		long long Bc>
	struct _colormatrix
	{
		const static long long y_r = r*Sl;
		const static long long y_g = (c-r-b)*Sl;
		const static long long y_b = b*Sl;
		const static long long y_c = Bl*S*c+S*c/2;
		const static long long y_d = S*c;
		const static long long cb_r = -r*Sc;
		const static long long cb_g = -(c-r-b)*Sc;
		const static long long cb_b = (c-b)*Sc;
		const static long long cb_c = 2*S*(c-b)*Bc+S*(c-b);
		const static long long cb_d = 2*S*(c-b);
		const static long long cr_r = (c-r)*Sc;
		const static long long cr_g = -(c-r-b)*Sc;
		const static long long cr_b = -b*Sc;
		const static long long cr_c = 2*S*(c-r)*Bc+S*(c-r);
		const static long long cr_d = 2*S*(c-r);
	};

	typedef _colormatrix<114,299,1000,255,65534,65534,0,32768> pc601;
	typedef _colormatrix<722,2126,10000,255,65534,65534,0,32768> pc709;
	typedef _colormatrix<114,299,1000,255,56064,57344,4096,32768> rec601;
	typedef _colormatrix<722,2126,10000,255,56064,57344,4096,32768> rec709;
	typedef _colormatrix<114,299,1000,255,254,254,0,128> pc601_8;
	typedef _colormatrix<722,2126,10000,255,254,254,0,128> pc709_8;
	typedef _colormatrix<114,299,1000,255,219,224,16,128> rec601_8;
	typedef _colormatrix<722,2126,10000,255,219,224,16,128> rec709_8;
	typedef _colormatrix<593,2627,10000,255,65534,65534,0,32768> pc2020;
	typedef _colormatrix<593,2627,10000,255,56064,57344,4096,32768> rec2020;
	typedef _colormatrix<593,2627,10000,255,254,254,0,128> pc2020_8;
	typedef _colormatrix<593,2627,10000,255,219,224,16,128> rec2020_8;

	template<class matrix, class matrix_8, size_t p, size_t r, size_t g, size_t b>
	struct _converter : public yuvc_converter
	{
		void Y(unsigned short* out, const unsigned char* in, size_t pixels)
		{
			for(size_t i = 0; i < pixels; i++)
				out[i] = (matrix::y_r * in[p * i + r] + matrix::y_g * in[p * i + g] +
					matrix::y_b * in[p * i + b] + matrix::y_c) / matrix::y_d;
		}

		void Cb(unsigned short* out, const unsigned char* in, size_t pixels)
		{
			for(size_t i = 0; i < pixels; i++)
				out[i] = (matrix::cb_r * in[p * i + r] + matrix::cb_g * in[p * i + g] +
					matrix::cb_b * in[p * i + b] + matrix::cb_c) / matrix::cb_d;
		}

		void Cr(unsigned short* out, const unsigned char* in, size_t pixels)
		{
			for(size_t i = 0; i < pixels; i++)
				out[i] = (matrix::cr_r * in[p * i + r] + matrix::cr_g * in[p * i + g] +
					matrix::cr_b * in[p * i + b] + matrix::cr_c) / matrix::cr_d;
		}

		void Y(unsigned char* out, const unsigned char* in, size_t pixels)
		{
			for(size_t i = 0; i < pixels; i++)
				out[i] = (matrix_8::y_r * in[p * i + r] + matrix_8::y_g * in[p * i + g] +
					matrix_8::y_b * in[p * i + b] + matrix_8::y_c) / matrix_8::y_d;
		}

		void Cb(unsigned char* out, const unsigned char* in, size_t pixels)
		{
			for(size_t i = 0; i < pixels; i++)
				out[i] = (matrix_8::cb_r * in[p * i + r] + matrix_8::cb_g * in[p * i + g] +
					matrix_8::cb_b * in[p * i + b] + matrix_8::cb_c) / matrix_8::cb_d;
		}

		void Cr(unsigned char* out, const unsigned char* in, size_t pixels)
		{
			for(size_t i = 0; i < pixels; i++)
				out[i] = (matrix_8::cr_r * in[p * i + r] + matrix_8::cr_g * in[p * i + g] +
					matrix_8::cr_b * in[p * i + b] + matrix_8::cr_c) / matrix_8::cr_d;
		}
	};

	template<class matrix, class matrix_8> yuvc_converter* get_converter(yuvc_rgbtype type)
	{
		switch(type) {
		case RGB_RGB: return new _converter<matrix, matrix_8, 3, 0, 1, 2>;
		case RGB_BGR: return new _converter<matrix, matrix_8, 3, 2, 1, 0>;
		case RGB_RGBX: return new _converter<matrix, matrix_8, 4, 0, 1, 2>;
		case RGB_BGRX: return new _converter<matrix, matrix_8, 4, 2, 1, 0>;
		case RGB_XRGB: return new _converter<matrix, matrix_8, 4, 1, 2, 3>;
		case RGB_XBGR: return new _converter<matrix, matrix_8, 4, 3, 2, 1>;
		};
		return NULL;
	}
}

yuvc_converter* yuvc_get_converter(yuvc_rgbtype type, yuvc_csptype csp)
{
	switch(csp) {
	case CSP_PC601: return get_converter<pc601, pc601_8>(type);
	case CSP_PC709: return get_converter<pc709, pc709_8>(type);
	case CSP_REC601: return get_converter<rec601, rec601_8>(type);
	case CSP_REC709: return get_converter<rec709, rec709_8>(type);
	case CSP_REC2020: return get_converter<rec2020, rec2020_8>(type);
	case CSP_PC2020: return get_converter<rec2020, rec2020_8>(type);
	};
	return NULL;
}

yuvc_converter* yuvc_get_converter(yuvc_rgbtype type, const std::string& csp)
{
	if(csp == "rec601") return get_converter<rec601, rec601_8>(type);
	if(csp == "pc601") return get_converter<pc601, pc601_8>(type);
	if(csp == "rec709") return get_converter<rec709, rec709_8>(type);
	if(csp == "pc709") return get_converter<pc709, pc709_8>(type);
	if(csp == "rec2020") return get_converter<rec2020, rec2020_8>(type);
	if(csp == "pc2020") return get_converter<pc2020, pc2020_8>(type);
	return NULL;
}

template<typename outv, typename inv>
void yuvc_shrink_croma(outv* out, const inv* in, size_t width, size_t height, bool gauss)
{
	if((width | height) & 1)
		throw std::runtime_error("yuvc_shrink_croma: Width and height must be multiple of 2");
	size_t pixels = width * height;
	size_t pixelsq = pixels / 4;
	init_tables();
	reduce_luma(out, in, width, height);
	if(gauss) {
		reduce_chroma_gauss(out + pixels, in + pixels, width, height);
		reduce_chroma_gauss(out + pixels + pixelsq, in + 2 * pixels, width, height);
	} else {
		reduce_chroma_bilinear(out + pixels, in + pixels, width, height);
		reduce_chroma_bilinear(out + pixels + pixelsq, in + 2 * pixels, width, height);
	}
}

template void yuvc_shrink_croma(uint8_t* out, const uint8_t* in, size_t width, size_t height, bool gauss);
template void yuvc_shrink_croma(uint8_t* out, const uint16_t* in, size_t width, size_t height, bool gauss);
template void yuvc_shrink_croma(uint16_t* out, const uint8_t* in, size_t width, size_t height, bool gauss);
template void yuvc_shrink_croma(uint16_t* out, const uint16_t* in, size_t width, size_t height, bool gauss);

void yuvc_magify_luma(unsigned char* out, const unsigned char* in, size_t width, size_t height)
{
	size_t pixels = width * height;
	for(size_t y = 0; y < height; y++) {
		for(size_t x = 0; x < width; x++)
			out[2 * x + 1] = out[2 * x] = in[x];
		memcpy(out + 2 * width, out, 2 * width);
		out += 4 * width;
		in += width;
	}
	memcpy(out, in, 2 * pixels);
}
