#include "yuv4mpeg.hpp"
#include "yuvconvert.hpp"
#include "parseval.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	bool ibits16 = false;
	bool obits16 = false;
	bool bilinear = false;
	bool magnify = false;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--16bit", arg)) {
			obits16 = true;
		} else if(r = regex("--bilinear", arg)) {
			bilinear = true;
		} else if(r = regex("--double", arg)) {
			magnify = true;
		} else {
			std::cerr << "subsample: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);

	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "444")
			ibits16 = false;
		else if(strmh.chroma == "444p16")
			ibits16 = true;
		else
			throw std::runtime_error("Unsupported input chroma type (need 8bit444 or 16bit444)");
		if(magnify && (ibits16 || obits16))
			throw std::runtime_error("Magnify is only supported for 8-bit");
		if(!magnify && (strmh.width | strmh.height) & 1)
			throw std::runtime_error("Input width and height must be even");
		strmh.chroma = obits16 ? "420p16" : "420";
		if(magnify) {
			strmh.width *= 2;
			strmh.height *= 2;
		}
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		std::vector<char> buffer;
		std::vector<char> buffer3;
		if(!magnify)
			buffer.resize((ibits16 ? 2 : 1) * 3 * strmh.width * strmh.height);
		else
			buffer.resize(3 * strmh.width * strmh.height / 4);
		buffer3.resize((obits16 ? 2 : 1) * 3 * strmh.width * strmh.height / 2);
		//Write frames.
		while(true) {
			std::string _framh;
			if(!read_line2(in, _framh))
				break;
			read_or_die(in, &buffer[0], buffer.size());
			struct yuv4mpeg_frame_header framh(_framh);
			if(strmh.interlace == 'm')
				framh.chroma = 'p';
			if(magnify)
				yuvc_magify_luma(reinterpret_cast<uint8_t*>(&buffer3[0]),
					reinterpret_cast<const uint8_t*>(&buffer[0]), strmh.width / 2,
					strmh.height / 2);
			else if(ibits16 && obits16)
				yuvc_shrink_croma(reinterpret_cast<uint16_t*>(&buffer3[0]),
					reinterpret_cast<const uint16_t*>(&buffer[0]), strmh.width, strmh.height,
					!bilinear);
			else if(ibits16 && !obits16)
				yuvc_shrink_croma(reinterpret_cast<uint8_t*>(&buffer3[0]),
					reinterpret_cast<const uint16_t*>(&buffer[0]), strmh.width, strmh.height,
					!bilinear);
			else if(!ibits16 && obits16)
				yuvc_shrink_croma(reinterpret_cast<uint16_t*>(&buffer3[0]),
					reinterpret_cast<const uint8_t*>(&buffer[0]), strmh.width, strmh.height,
					!bilinear);
			else if(!ibits16 && !obits16)
				yuvc_shrink_croma(reinterpret_cast<uint8_t*>(&buffer3[0]),
					reinterpret_cast<const uint8_t*>(&buffer[0]), strmh.width, strmh.height,
					!bilinear);
			write_or_die(out, _framh);
			write_or_die(out, &buffer3[0], buffer3.size());
		}
	} catch(std::exception& e) {
		std::cerr << "subsample: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
