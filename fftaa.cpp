#include <limits>
#include <cstdint>
#include <complex>
#include <vector>
#include <fftw3.h>
#include <cstring>
#include <stdexcept>
#include <iostream>
#include "parseval.hpp"
#include "yuv4mpeg.hpp"


fftw_plan forward_plan;
fftw_plan backward_plan;
fftw_plan forward_plan2;
fftw_plan backward_plan2;
std::complex<double>* input_memory;
std::complex<double>* output_memory;

void allocate_ffts(size_t iw, size_t ih, size_t ow, size_t oh, size_t wf, size_t hf)
{
	input_memory = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(iw * ih));
	output_memory = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(iw * ih));
	forward_plan = fftw_plan_dft_2d(ih, iw, reinterpret_cast<fftw_complex*>(input_memory), 
		reinterpret_cast<fftw_complex*>(output_memory), -1, FFTW_MEASURE);
	backward_plan = fftw_plan_dft_2d(oh, ow, reinterpret_cast<fftw_complex*>(input_memory), 
		reinterpret_cast<fftw_complex*>(output_memory), +1, FFTW_MEASURE);
	forward_plan2 = fftw_plan_dft_2d(ih / wf, iw / hf, reinterpret_cast<fftw_complex*>(input_memory), 
		reinterpret_cast<fftw_complex*>(output_memory), -1, FFTW_MEASURE);
	backward_plan2 = fftw_plan_dft_2d(oh / wf, ow / hf, reinterpret_cast<fftw_complex*>(input_memory), 
		reinterpret_cast<fftw_complex*>(output_memory), +1, FFTW_MEASURE);
}

void copyrow(std::complex<double>* target, const std::complex<double>* source, size_t isize, size_t osize)
{
	size_t lsize = (osize + 1) >> 1;
	size_t rsize = (osize - 1) >> 1;
	memcpy(target, source, lsize * sizeof(std::complex<double>));
	memcpy(target + osize - rsize, source + isize - rsize, rsize * sizeof(std::complex<double>));
	if((osize & 1) == 0)
		target[osize / 2] = 0;
}

void copyimage(std::complex<double>* target, std::complex<double>* source, size_t isize, size_t osize,
	size_t irows, size_t orows)
{
	size_t usize = (orows + 1) >> 1;
	size_t lsize = (orows - 1) >> 1;

	for(size_t i = 0; i < usize; i++)
		copyrow(target + i * osize, source + i * isize, isize, osize);
	for(size_t i = 0; i < lsize; i++)
		copyrow(target + (orows - lsize + i) * osize, source + (irows - lsize + i) * isize, isize, osize);
	if((orows & 1) == 0)
		for(size_t i = 0; i < osize; i++)
			target[(orows / 2) * osize + i] = 0;
}

template<bool small> void fft_aa_plane(double* obuffer, const double* ibuffer, size_t width, size_t height,
	size_t istride, size_t ostride, size_t owidth, size_t oheight)
{
	double oscale = static_cast<double>(width) * height;
	for(size_t i = 0; i < height; i++)
		for(size_t j = 0; j < width; j++)
			input_memory[i * width + j] = ibuffer[i * istride + j];
	if(small)
		fftw_execute(forward_plan2);
	else
		fftw_execute(forward_plan);
	copyimage(input_memory, output_memory, width, owidth, height, oheight);
	if(small)
		fftw_execute(backward_plan2);
	else
		fftw_execute(backward_plan);

	for(size_t i = 0; i < oheight; i++)
		for(size_t j = 0; j < owidth; j++)
			obuffer[i * ostride + j] = std::abs(output_memory[i * owidth + j]) / oscale;
}

template<typename T> inline double integer_to_float(T val)
{
	uint64_t v = ((0x3FFULL + 8 * sizeof(T)) << 52) + (static_cast<uint64_t>(val) << (52 - 8 * sizeof(T)));
	double* v2 = reinterpret_cast<double*>(&v);
	return *v2;
}

template<typename T> inline T float_to_integer(double val)
{
	const uint64_t low = ((0x3FFULL + 8 * sizeof(T)) << 52);
	const uint64_t high = ((0x400ULL + 8 * sizeof(T)) << 52);
	uint64_t* v2 = reinterpret_cast<uint64_t*>(&val);
	uint64_t v = *v2;
	//Mark braches for underflow/overflow as unlikely.
	if(__builtin_expect(v < low, 0))
		return 0;
	if(__builtin_expect(v >= high, 0))
		return std::numeric_limits<T>::max();
	return (v >> (52 - 8 * sizeof(T)));
}

template<typename T, unsigned bpp, unsigned shift, bool small> void fft_aa_plane(T* obuffer, const T* ibuffer,
	size_t width, size_t height, size_t istride, size_t ostride, size_t owidth, size_t oheight)
{
	static std::vector<double> in;
	static std::vector<double> out;
	if(in.size() != height * istride) {
		in.resize(height * istride);
		out.resize(oheight * ostride);
	}
	for(size_t i = 0; i < height * istride; i++)
		in[i] = integer_to_float<T>(ibuffer[bpp * i + shift]);
	for(size_t i = 0; i < height * istride; i++)
		in[i] -= (1ULL << (8 * sizeof(T)));
	fft_aa_plane<small>(&out[0], &in[0], width, height, istride, ostride, owidth, oheight);
	for(size_t i = 0; i < oheight * ostride; i++)
		out[i] += (1ULL << (8 * sizeof(T)));
	for(size_t i = 0; i < oheight * ostride; i++)
		obuffer[bpp * i + shift] = float_to_integer<T>(out[i]);
}

inline void fft_aa_rgb(uint8_t* obuffer, const uint8_t* ibuffer, size_t width, size_t height, size_t owidth,
	size_t oheight)
{
	fft_aa_plane<uint8_t, 3, 0, false>(obuffer, ibuffer, width, height, width, owidth, owidth, oheight);
	fft_aa_plane<uint8_t, 3, 1, false>(obuffer, ibuffer, width, height, width, owidth, owidth, oheight);
	fft_aa_plane<uint8_t, 3, 2, false>(obuffer, ibuffer, width, height, width, owidth, owidth, oheight);
}

template<typename T, size_t w, size_t h> inline void fft_aa_yuv(T* obuffer, const T* ibuffer, size_t width,
	size_t height, size_t owidth, size_t oheight)
{
	size_t ilsize = width * height;
	size_t icsize = width * height / w / h;
	size_t olsize = owidth * oheight;
	size_t ocsize = owidth * oheight / w / h;
	size_t cwidth = width / w;
	size_t cheight = height / h;
	size_t ocwidth = owidth / w;
	size_t ocheight = oheight / h;
	if(width % w || height % h || owidth % w || oheight % h)
		throw std::runtime_error("Size must be multiple of chroma block size");

	fft_aa_plane<T, 1, 0, false>(obuffer, ibuffer, width, height, width, owidth, owidth, oheight);
	fft_aa_plane<T, 1, 0, true>(obuffer + olsize, ibuffer + ilsize, cwidth, cheight, cwidth, ocwidth, ocwidth,
		ocheight);
	fft_aa_plane<T, 1, 0, true>(obuffer + olsize + ocsize, ibuffer + ilsize + icsize, cwidth, cheight, cwidth,
		ocwidth, ocwidth, ocheight);
}

typedef void (*fft_scale_fun_t)(void* obuffer, const void* ibuffer, size_t width, size_t height, size_t owidth,
	size_t oheight);

template<typename T, size_t w, size_t h, bool rgb> inline void fft_aa2(void* obuffer, const void* ibuffer,
	size_t width, size_t height, size_t owidth, size_t oheight)
{
	if(rgb)
		fft_aa_rgb(reinterpret_cast<uint8_t*>(obuffer), reinterpret_cast<const uint8_t*>(ibuffer),
			width, height, owidth, oheight);
	else
		fft_aa_yuv<T, w, h>(reinterpret_cast<T*>(obuffer), reinterpret_cast<const T*>(ibuffer), width, height,
			owidth, oheight);
}

int main(int argc, char** argv)
{
	size_t outwidth = 0;
	size_t outheight = 0;
	size_t hscale = 0;
	size_t vscale = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--resolution=([1-9][0-9]*)x([1-9][0-9]*)", arg)) {
			try {
				outwidth = parse_value<unsigned>(r[1]);
				outheight = parse_value<unsigned>(r[2]);
			} catch(std::exception& e) {
				std::cerr << "fftaa: Bad resolution '" << r[1] << "x" << r[2] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--scale=([1-9][0-9]*)x([1-9][0-9]*)", arg)) {
			try {
				hscale = parse_value<unsigned>(r[1]);
				vscale = parse_value<unsigned>(r[2]);
			} catch(std::exception& e) {
				std::cerr << "fftaa: Bad scale '" << r[1] << "x" << r[2] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--scale=([1-9][0-9]*)", arg)) {
			try {
				hscale = parse_value<unsigned>(r[1]);
				vscale = parse_value<unsigned>(r[1]);
			} catch(std::exception& e) {
				std::cerr << "fftaa: Bad scale '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else {
			std::cerr << "fftaa: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}
	if((!outwidth || !outheight) && (!hscale || !vscale)) {
		std::cerr << "fftaa: Resolution needed" << std::endl;
		return 1;
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		size_t inwidth = strmh.width;
		size_t inheight = strmh.height;

		if(hscale && vscale) {
			outwidth = inwidth / hscale;
			outheight = inheight / vscale;
		}

		strmh.width = outwidth;
		strmh.height = outheight;

		if(inwidth < outwidth || inheight < outheight) {
			std::cerr << "fftaa: Only scaling to smaller is supported" << std::endl;
			return 1;
		}

		fft_scale_fun_t fun;
		size_t quads;
		size_t wf = 1;
		size_t hf = 1;

		if(strmh.chroma == "rgb") {
			quads = 12;
			fun = fft_aa2<uint8_t, 1, 1, true>;
		} else if(strmh.chroma == "420") {
			quads = 6;
			fun = fft_aa2<uint8_t, 2, 2, false>;
			hf = wf = 2;
		} else if(strmh.chroma == "420p16") {
			quads = 12;
			fun = fft_aa2<uint16_t, 2, 2, false>;
			hf = wf = 2;
		} else if(strmh.chroma == "422") {
			quads = 8;
			fun = fft_aa2<uint8_t, 2, 1, false>;
			wf = 2;
		} else if(strmh.chroma == "422p16") {
			quads = 16;
			fun = fft_aa2<uint16_t, 2, 1, false>;
			wf = 2;
		} else if(strmh.chroma == "444") {
			quads = 12;
			fun = fft_aa2<uint8_t, 1, 1, false>;
		} else if(strmh.chroma == "444p16") {
			quads = 24;
			fun = fft_aa2<uint16_t, 1, 1, false>;
		} else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		allocate_ffts(inwidth, inheight, outwidth, outheight, wf, hf);

		std::vector<char> buffer;
		std::vector<char> buffer2;
		size_t insize = inwidth * inheight * quads / 4;
		size_t outsize = outwidth * outheight * quads / 4;
		buffer.resize(insize + 128);
		buffer2.resize(outsize + 128);
		while(true) {
			std::string _framh;
			if(!read_line2(in, _framh))
				break;
			read_or_die(in, &buffer[0], insize);
			fun(&buffer2[0], &buffer[0], inwidth, inheight, outwidth, outheight);
			write_or_die(out, _framh);
			write_or_die(out, &buffer2[0], outsize);
			
		}
	} catch(std::exception& e) {
		std::cerr << "fftaa: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
