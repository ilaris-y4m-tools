#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include <iostream>
extern "C"
{
#ifndef UINT64_C
#define UINT64_C(val) val##ULL
#endif
#include <libswscale/swscale.h>
}

namespace
{
	PixelFormat sws_fmt;

	void init_tables()
	{
		uint16_t magic = 258;
		sws_fmt = (*reinterpret_cast<uint8_t*>(&magic) == 2) ? PIX_FMT_GRAY16LE : PIX_FMT_GRAY16BE;
	}

	struct scale_params
	{
		size_t inwidth;
		size_t inheight;
		size_t outwidth;
		size_t outheight;
		std::string chroma;
		int flags;
		int flags2;
		void fill_csp()
		{
			init_tables();
			size_t pfourths;
			size_t p1fourths;
			size_t pnfourths;
			bool subsample_x = false;
			bool subsample_y = false;
			if(chroma == "rgb") {
				pfourths = 12;
				p1fourths = 12;
				pnfourths = 0;
				planes = 1;
				mode = 0;
			} else if(chroma == "420") {
				pfourths = 6;
				p1fourths = 4;
				pnfourths = 1;
				planes = 3;
				subsample_x = true;
				subsample_y = true;
				mode = 1;
			} else if(chroma == "420p16") {
				pfourths = 12;
				p1fourths = 8;
				pnfourths = 2;
				planes = 3;
				subsample_x = true;
				subsample_y = true;
				mode = 2;
			} else if(chroma == "422") {
				pfourths = 8;
				p1fourths = 4;
				pnfourths = 2;
				planes = 3;
				subsample_x = true;
				mode = 1;
			} else if(chroma == "422p16") {
				pfourths = 16;
				p1fourths = 8;
				pnfourths = 4;
				planes = 3;
				subsample_x = true;
				mode = 2;
			} else if(chroma == "444") {
				pfourths = 12;
				p1fourths = 4;
				pnfourths = 4;
				planes = 3;
				mode = 1;
			} else if(chroma == "444p16") {
				pfourths = 24;
				p1fourths = 8;
				pnfourths = 8;
				planes = 3;
				mode = 2;
			} else
				throw std::runtime_error("Unsupported input chroma type '" + chroma + "'");
			insize = pfourths * inwidth * inheight / 4;
			outsize = pfourths * outwidth * outheight / 4;
			inplane1_size = p1fourths * inwidth * inheight / 4;
			outplane1_size = p1fourths * outwidth * outheight / 4;
			inplanen_size = pnfourths * inwidth * inheight / 4;
			outplanen_size = pnfourths * outwidth * outheight / 4;
			inplane1_stride = p1fourths * inwidth / 4;
			outplane1_stride = p1fourths * outwidth / 4;
			//Hack: Multiply plane_n strides by 2 if vertically subsampled, so that the values are
			//correct.
			size_t hack_multiply = subsample_y ? 2 : 1;
			inplanen_stride = hack_multiply * pnfourths * inwidth / 4;
			outplanen_stride = hack_multiply * pnfourths * outwidth / 4;
			if(subsample_x && (inwidth | outwidth) & 1)
				throw std::runtime_error("420 and 422 modes must have even width");
			if(subsample_y && (inheight | outheight) & 1)
				throw std::runtime_error("420 modes must have even height");
			inplanen_width = inwidth / (subsample_x ? 2 : 1);
			inplanen_height = inheight / (subsample_y ? 2 : 1);
			outplanen_width = outwidth / (subsample_x ? 2 : 1);
			outplanen_height = outheight / (subsample_y ? 2 : 1);
			if(!flags2)
				flags2 = flags;
		}
		int mode;
		size_t insize;
		size_t outsize;
		size_t inplane1_size;
		size_t inplanen_size;
		size_t outplane1_size;
		size_t outplanen_size;
		size_t inplane1_stride;
		size_t inplanen_stride;
		size_t outplane1_stride;
		size_t outplanen_stride;
		size_t inplanen_width;
		size_t inplanen_height;
		size_t outplanen_width;
		size_t outplanen_height;
		unsigned planes;
	};

/*  Debugging aid, commented out since compiler does not like unused defintions.
#define PRINTFIELD(X) << #X "=" << s. X << " "

	std::ostream& operator<<(std::ostream& os, const scale_params& s)
	{
		os 
			PRINTFIELD(inwidth)
			PRINTFIELD(inheight)
			PRINTFIELD(outwidth)
			PRINTFIELD(outheight)
			PRINTFIELD(chroma)
			PRINTFIELD(flags)
			PRINTFIELD(flags2)
			PRINTFIELD(mode)
			PRINTFIELD(insize)
			PRINTFIELD(outsize)
			PRINTFIELD(inplane1_size)
			PRINTFIELD(inplanen_size)
			PRINTFIELD(outplane1_size)
			PRINTFIELD(outplanen_size)
			PRINTFIELD(inplane1_stride)
			PRINTFIELD(inplanen_stride)
			PRINTFIELD(outplane1_stride)
			PRINTFIELD(outplanen_stride)
			PRINTFIELD(inplanen_width)
			PRINTFIELD(outplanen_width)
			PRINTFIELD(inplanen_height)
			PRINTFIELD(outplanen_height)
			PRINTFIELD(planes)
			;
		return os;
	}

#undef PRINTFIELD
*/

	void do_resize(char* out, const char* in, struct scale_params& s)
	{
		PixelFormat pfmt;
		if(s.mode == 0)
			pfmt = PIX_FMT_RGB24;
		if(s.mode == 1)
			pfmt = PIX_FMT_GRAY8;
		if(s.mode == 2)
			pfmt = sws_fmt;
		//Do plane1 first.
		static SwsContext* ctx1 = NULL;
		ctx1 = sws_getCachedContext(ctx1, s.inwidth, s.inheight, pfmt, s.outwidth, s.outheight, pfmt, s.flags,
			NULL, NULL, NULL);
		const uint8_t* in_ptr[1];
		int in_stride[1];
		uint8_t* out_ptr[1];
		int out_stride[1];
		in_stride[0] = s.inplane1_stride;
		out_stride[0] = s.outplane1_stride;
		in_ptr[0] = reinterpret_cast<const uint8_t*>(in);
		out_ptr[0] = reinterpret_cast<uint8_t*>(out);
		sws_scale(ctx1, in_ptr, in_stride, 0, s.inheight, out_ptr, out_stride);
		for(unsigned i = 0; i < s.planes - 1; i++) {
			static SwsContext* ctx2 = NULL;
			ctx2 = sws_getCachedContext(ctx2, s.inplanen_width, s.inplanen_height, pfmt,
				s.outplanen_width, s.outplanen_height, pfmt, s.flags2, NULL, NULL, NULL);
			in_stride[0] = s.inplanen_stride;
			out_stride[0] = s.outplanen_stride;
			in_ptr[0] = reinterpret_cast<const uint8_t*>(in + s.inplane1_size + i * s.inplanen_size);
			out_ptr[0] = reinterpret_cast<uint8_t*>(out + s.outplane1_size + i * s.outplanen_size);
			sws_scale(ctx2, in_ptr, in_stride, 0, s.inplanen_height, out_ptr, out_stride);
		}
	}

	void compute_scale(size_t inw, size_t inh, size_t& outw, size_t& outh, size_t xf, size_t yf, bool xd,
		bool yd, bool pc, uint32_t& sn, uint32_t& sd)
	{
		if(!outw || !outh) {
			outw = xd ? (inw / xf) : (inw * xf);
			outh = yd ? (inh / yf) : (inh * yf);
		}
		if(!outw || !outh) {
			outw = inw;
			outh = inh;
		}
		if(pc) {
			get_512_scaling(outw, outh, sn, sd, outw, outh);
			sn = 1;
			sd = 1;
		}
	}
}

int main(int argc, char** argv)
{
	scale_params s;
	s.inwidth = 0;
	s.inheight = 0;
	s.outwidth = 0;
	s.outheight = 0;
	size_t xfactor = 0;
	bool xdown = false;
	size_t yfactor = 0;
	bool ydown = false;
	s.flags = 4;
	s.flags2 = 0;
	bool precorrect = false;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--precorrect", arg)) {
			precorrect = true;
		} else if(r = regex("--resolution=([1-9][0-9]*)x([1-9][0-9]*)", arg)) {
			try {
				s.outwidth = parse_value<unsigned>(r[1]);
				s.outheight = parse_value<unsigned>(r[2]);
			} catch(std::exception& e) {
				std::cerr << "resize: Bad resolution '" << r[1] << "x" << r[2] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--factor=([/x])([1-9][0-9]*)", arg)) {
			try {
				xfactor = yfactor = parse_value<size_t>(r[2]);
				xdown = ydown = (r[1] == "/");
			} catch(std::exception& e) {
				std::cerr << "resize: Bad factor '" << arg << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--factor=([/x])([1-9][0-9]*)x([/x])([1-9][0-9]*)", arg)) {
			try {
				xfactor = parse_value<size_t>(r[2]);
				yfactor = parse_value<size_t>(r[4]);
				xdown = (r[1] == "/");
				ydown = (r[3] == "/");
			} catch(std::exception& e) {
				std::cerr << "resize: Bad factor '" << arg << "'" << std::endl;
				return 1;
			}
		} else if(arg == "--fast-bilinear") {
			s.flags = SWS_FAST_BILINEAR;
		} else if(arg == "--bilinear") {
			s.flags = SWS_BILINEAR;
		} else if(arg == "--bicubic") {
			s.flags = SWS_BICUBIC;
		} else if(arg == "--experimential") {
			s.flags = SWS_X;
		} else if(arg == "--point") {
			s.flags = SWS_POINT;
		} else if(arg == "--area") {
			s.flags = SWS_AREA;
		} else if(arg == "--bicubic-linear") {
			s.flags = SWS_BICUBIC;
			s.flags2 = SWS_BILINEAR;
		} else if(arg == "--gauss") {
			s.flags = SWS_GAUSS;
		} else if(arg == "--sinc") {
			s.flags = SWS_SINC;
		} else if(arg == "--lanczos") {
			s.flags = SWS_LANCZOS;
		} else if(arg == "--spline") {
			s.flags = SWS_SPLINE;
		} else {
			std::cerr << "resize: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}
	if((!s.outwidth || !s.outheight) && (!xfactor || !yfactor) && !precorrect) {
		std::cerr << "resize: Resolution or factor needed if not precorrecting" << std::endl;
		return 1;
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		s.inwidth = strmh.width;
		s.inheight = strmh.height;
		compute_scale(s.inwidth, s.inheight, s.outwidth, s.outheight, xfactor, yfactor, xdown, ydown,
			precorrect, strmh.sar_n, strmh.sar_d);
		strmh.width = s.outwidth;
		strmh.height = s.outheight;
		
		s.chroma = strmh.chroma;
		s.fill_csp();

		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		std::vector<char> buffer;
		std::vector<char> buffer2;
		buffer.resize(s.insize);
		buffer2.resize(s.outsize);
		while(true) {
			std::string _framh;
			if(!read_line2(in, _framh))
				break;
			read_or_die(in, &buffer[0], s.insize);
			do_resize(&buffer2[0], &buffer[0], s);
			write_or_die(out, _framh);
			write_or_die(out, &buffer2[0], s.outsize);
			
		}
	} catch(std::exception& e) {
		std::cerr << "resize: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
