#include "rendertext.hpp"
#include "parseval.hpp"
#include "yuv4mpeg.hpp"
#include "yuvconvert.hpp"
#include <fstream>
#include <iostream>
#include <ft2build.h>
#include FT_FREETYPE_H

namespace
{
	static bool ft_init;
	static FT_Library ft_handle;

	void mix(uint16_t& a, uint16_t b, uint32_t t)
	{
		a = (a * (65536 - t) + b * t) >> 16;
	}

	void mix(uint8_t& a, uint8_t b, uint32_t t)
	{
		a = (a * (256 - t) + b * t) >> 8;
	}

	void render_halo(uint8_t* dest, const uint8_t* src, size_t width, size_t height, signed thickness)
	{
		if(dest != src)
			memcpy(dest, src, width * height);
		for(unsigned j = 0; j < height; j++)
			for(unsigned i = 0; i < width; i++)
				if(dest[j * width + i] == 1)
					for(signed y = -thickness; y <= thickness; y++) {
						if(j + y < 0)
							continue;
						if(j + y >= height)
							break;
						for(signed x = -thickness; x <= thickness; x++) {
							if(i + x < 0)
								continue;
							if(i + x >= width)
								break;
							if(dest[(j + y) * width + (i + x)] == 0)
								dest[(j + y) * width + (i + x)] = 2;
						}
					}
	}

	struct errmsg
	{
		int code;
		const char* message;
	};

	void throw_ft_error(FT_Error code)
	{
		if(!code)
			return;
	//There's no built-in error table in Freetype 2???
#undef __FTERRORS_H__
#define FT_ERRORDEF(e, v, s) {e, s},
#define FT_ERROR_START_LIST {
#define FT_ERROR_END_LIST {0, 0}};
	static struct errmsg freetype_errors[] =
#include FT_ERRORS_H
		struct errmsg* i = freetype_errors;
		while(i->message) {
			if(i->code == code)
				throw std::runtime_error(std::string("Freetype2 error: ") + i->message);
			i++;
		}
		std::ostringstream s;
		s << "Freetype2 error: Unknown error #" << code;
		throw std::runtime_error(s.str());
	}

	struct render_line_info
	{
		size_t width;
		size_t height;
		size_t baseline;
		size_t offset;
	};

	struct render_line_info ft_rendered_size_line(FT_Face face, const std::string& text)
	{
		struct render_line_info l;
		uint16_t state = utf8_initial_state;
		size_t len = text.length();
		size_t basepos = 0;
		l.width = l.height = l.baseline = l.offset = 0;
		for(size_t i = 0; i <= len; i++) {
			ssize_t base;
			int32_t sym = utf8_parse_byte((i < len) ? static_cast<uint8_t>(text[i]) : -1, state);
			if(sym < 0)
				continue;
			//Got unicode symbol.
			unsigned gslot = FT_Get_Char_Index(face, sym);
			if(!gslot) {
				//Missing symbol!
				std::cerr << "Warning: Symbol " << sym << " not present in font!" << std::endl;
				continue;
			}
			try {
				throw_ft_error(FT_Load_Glyph(face, gslot, FT_LOAD_DEFAULT));
				throw_ft_error(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_MONO));
			} catch(std::exception& e) {
				std::cerr << "Warning: Symbol " << sym << " can't be rendered: " << e.what()
					<< std::endl;
				continue;
			}
			//If offset needs adjustment, do that.
			base = basepos + face->glyph->bitmap_left;
			if(base < 0 && l.offset < static_cast<size_t>(-base))
				l.offset = static_cast<size_t>(-base);
			//If baseline need adjustment, do that.
			base = -face->glyph->bitmap_top;
			if(base < 0 && l.baseline < static_cast<size_t>(-base))
				l.baseline = static_cast<size_t>(-base);
			//Compute width.
			base = basepos + face->glyph->bitmap_left + (ssize_t)face->glyph->bitmap.width;
			if(base > static_cast<ssize_t>(l.width))
				l.width = base;
			//Compute height.
			base = -face->glyph->bitmap_top + (ssize_t)face->glyph->bitmap.rows;
			if(base > static_cast<ssize_t>(l.height))
				l.height = base;
			basepos = basepos + (face->glyph->advance.x >> 6);
		}
		//Width / height don't take negative excursions into account.
		l.width = l.width + l.offset;
		l.height = l.height + l.baseline;
		if(l.height > 1000000000 || l.width > 1000000000) {
			std::cerr << "Subtitle: Internal Error: Insane row size!" << std::endl;
			abort();
		}
		return l;
	}

	ssize_t align_position(size_t space, size_t size, int32_t pos)
	{
		ssize_t freespace = space - size;
		return ((pos + 1000) * freespace) / 2000;
	}
	
	void ft_render_text_line(FT_Face face, const std::string& text, std::vector<uint8_t>& data, size_t width,
		size_t height, ssize_t alignment, size_t y, struct render_line_info l)
	{
		ssize_t p = align_position(width, l.width, alignment);
		if(y >= height || p >= static_cast<ssize_t>(width))
			return;		//Nothing fits.
		ssize_t offset_x;
		ssize_t offset_y;
		ssize_t basepos = 0;
		uint16_t state = utf8_initial_state;
		size_t len = text.length();
		//We need to compute the window offset.
		offset_y = l.baseline + y;
		offset_x = l.offset + p;
		for(size_t i = 0; i <= len; i++) {
			int32_t sym = utf8_parse_byte((i < len) ? static_cast<uint8_t>(text[i]) : -1, state);
			if(sym < 0)
				continue;
			//Got unicode symbol.
			unsigned gslot = FT_Get_Char_Index(face, sym);
			if(!gslot) {
				//Missing symbol!
				std::cerr << "Warning: Symbol " << sym << " not present in font!" << std::endl;
				continue;
			}
			try {
				throw_ft_error(FT_Load_Glyph(face, gslot, FT_LOAD_DEFAULT));
				throw_ft_error(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_MONO));
			} catch(std::exception& e) {
				std::cerr << "Warning: Symbol " << sym << " can't be rendered: " << e.what()
					<< std::endl;
				continue;
			}
			ssize_t render_x = basepos + face->glyph->bitmap_left + offset_x;
			ssize_t render_y = -face->glyph->bitmap_top + offset_y;
			for(int j = 0; (unsigned)j < face->glyph->bitmap.rows && render_y + j <
					static_cast<ssize_t>(height); j++) {
				if(render_y + j < 0)
					continue;
				const uint8_t* ldata = face->glyph->bitmap.buffer + j * face->glyph->bitmap.pitch;
				uint8_t* tmpr = &data[(render_y + j) * width];
				for(int k = 0; (unsigned)k < face->glyph->bitmap.width && render_x + k <
					static_cast<ssize_t>(width); k++) {
					if(render_x + k < 0)
						continue;
					tmpr[render_x + k] = ((ldata[k >> 3] >> (7 - (k & 7))) & 1) ? 1 : 0;
				}
			}
			basepos = basepos + (face->glyph->advance.x >> 6);
		}
	}

	void ft_render_text(FT_Face face, const std::string& text, std::vector<uint8_t>& data, size_t width,
		size_t height, ssize_t alignment, size_t spacing, size_t y = 0)
	{
		std::string _text = text;
		//If the string contains linefeeds, break it up.
		size_t p = _text.find("\\n");
		if(p < _text.length()) {
			std::string t1 = _text.substr(0, p);
			std::string t2 = _text.substr(p + 2);
			auto g1 = ft_rendered_size_line(face, t1);
			ft_render_text_line(face, t1, data, width, height, alignment, y, g1);
			ft_render_text(face, t2, data, width, height, alignment, spacing, y + g1.height + spacing);
			return;
		}
		//The text is in one piece if this place is reached.
		auto g1 = ft_rendered_size_line(face, text);
		ft_render_text_line(face, text, data, width, height, alignment, y, g1);
	}

	std::pair<size_t, size_t> ft_rendered_size(FT_Face face, const std::string& text, size_t spacing)
	{
		std::string _text = text;
		//If the string contains linefeeds, break it up.
		size_t p = _text.find("\\n");
		if(p < _text.length()) {
			std::string t1 = _text.substr(0, p);
			std::string t2 = _text.substr(p + 2);
			auto g1 = ft_rendered_size_line(face, t1);
			auto g2 = ft_rendered_size(face, t2, spacing);
			size_t w, h;
			w = (g1.width > g2.first) ? g1.width : g2.first;
			h = g1.height + spacing + g2.second;
			return std::make_pair(w, h);
		}
		//The text is in one piece if this place is reached.
		auto g = ft_rendered_size_line(face, text);
		return std::make_pair(g.width, g.height);
	}

	std::string read_subtitle_from_file(std::string filename)
	{
		std::string content;
		std::string tmp;
		std::ifstream x(filename);
		if(!x)
			throw std::runtime_error("Can't open '" + filename + "'");
		while(std::getline(x, tmp)) {
			if(content != "")
				content = content + "\n" + tmp;
			else
				content = tmp;
		}
		std::ostringstream content2;
		for(size_t i = 0; i < content.length(); i++)
			if(content[i] == '\n')
				content2 << "\\n";
			else if(content[i] == '\\')
				content2 << "\\\\";
			else
				content2 << content[i];
		return content2.str();
	}

	ssize_t position_subtitle(size_t space, size_t size, int32_t pos, bool absolute)
	{
		if(absolute)
			return pos;
		else {
			ssize_t freespace = space - size;
			return ((pos + 1000) * freespace) / 2000;
		}
	}
	
	yuvc_converter* get_converter(const regex_results& r)
	{
		std::string csp = "rec601";
		if(r)
			csp = r[1];
		yuvc_converter* c = yuvc_get_converter(RGB_RGB, csp);
		if(c)
			return c;
		if(r)
			std::cerr << "Warning: Stream YUV variant unspecified, assuming rec601." << std::endl;
		else
			std::cerr << "Warning: Stream YUV variant unsupported, falling back to rec601." << std::endl;
		return yuvc_get_converter(RGB_RGB, "rec601");
	}

	void stamp_pos(size_t space, size_t dimension, ssize_t pos, size_t& fpos, size_t& spos, size_t& sdim)
	{
		sdim = dimension;
		if(pos < 0) {
			spos = -pos;
			if(sdim <= spos) {
				fpos = 0;
				spos = 0;
				sdim = 0;
				return;
			}
			sdim = sdim + pos;
			fpos = 0;
		} else {
			spos = 0;
			fpos = pos;
		}
		if(fpos > space)
			sdim = 0;
		else if(fpos + sdim > space)
			sdim = space - fpos;
	}
}

std::pair<size_t, size_t> text_render_parameters::render_size(const std::string& text)
{
	FT_Face face;

	//Init freetype2 if needed.
	if(!ft_init)
		throw_ft_error(FT_Init_FreeType(&ft_handle));
	ft_init = true;

	//Load the font face.
	if(fontfile == "")
		throw std::runtime_error("Font file needed for text rendering");
	throw_ft_error(FT_New_Face(ft_handle, fontfile.c_str(), fontface, &face));
	throw_ft_error(FT_Set_Pixel_Sizes(face, 0, fontsize));

	auto g = ft_rendered_size(face, text, text_spacing);
	return std::make_pair(g.first + 2 * halo_thickness, g.second + 2 * halo_thickness);
}

parsed_png& text_render_parameters::render(const std::string& text)
{
	FT_Face face;
	std::vector<uint8_t> tmp;
	size_t width, height;
	size_t width2, height2;

	//Init freetype2 if needed.
	if(!ft_init)
		throw_ft_error(FT_Init_FreeType(&ft_handle));
	ft_init = true;

	//Load the font face.
	if(fontfile == "")
		throw std::runtime_error("Font file needed for text rendering");
	throw_ft_error(FT_New_Face(ft_handle, fontfile.c_str(), fontface, &face));
	throw_ft_error(FT_Set_Pixel_Sizes(face, 0, fontsize));

	//First we need to figure out the size of the rendered bitmap. Then render the actual bitmap.
	auto g = ft_rendered_size(face, text, text_spacing);
	width = g.first;
	height = g.second;
	width2 = width + 2 * halo_thickness;
	height2 = height + 2 * halo_thickness;
	tmp.resize(width2 * height2);
	memset(&tmp[0], 0, width2 * height2);
	ft_render_text(face, text, tmp, width, height, text_alignment, text_spacing);

	//Do in-place padding by halo_thickness on all sides.
	if(halo_thickness > 0) {
		for(size_t i = height - 1; i < height; i--)
			memmove(&tmp[width2 * (i + halo_thickness) + halo_thickness], &tmp[width * i], width);
		for(size_t i = 0; i < height; i++) {
			memset(&tmp[width2 * (i + halo_thickness)], 0, halo_thickness);
			memset(&tmp[width2 * (i + halo_thickness + 1) - halo_thickness], 0, halo_thickness);
		}
		memset(&tmp[0], 0, width2 * halo_thickness);
		render_halo(&tmp[0], &tmp[0], width2, height2, halo_thickness);
	}

	parsed_png& png = *new parsed_png(width2, height2);
	for(size_t i = 0; i < width2 * height2; i++) {
		if(tmp[i] == 0) png.data[i] = bg_color;
		if(tmp[i] == 1) png.data[i] = fg_color;
		if(tmp[i] == 2) png.data[i] = halo_color;
	}
	return png;
}

text_render_parameters::text_render_parameters()
{
	fontsize = 12;
	text_alignment = 0;
	text_spacing = 0;
	fontface = 0;
	fg_color = 0xFFFFFFFFU;
	halo_color = 0xFF000000U;
	bg_color = 0;
	halo_thickness = 1;
}

bool text_render_parameters::argument(const std::string& arg)
{
	regex_results r;
	if(r = regex("--font-file=(.*)", arg)) {
		fontfile = r[1];
		return true;
	} else if(r = regex("--font-size=(.*)", arg)) {
		fontsize = parse_value<unsigned>(r[1]);
		return true;
	} else if(r = regex("--text-alignment=left", arg)) {
		text_alignment = -1000;
		return true;
	} else if(r = regex("--text-alignment=center", arg)) {
		text_alignment = 0;
		return true;
	} else if(r = regex("--text-alignment=right", arg)) {
		text_alignment = 1000;
		return true;
	} else if(r = regex("--text-alignment=(.*)", arg)) {
		text_alignment = parse_value<signed>(r[1]);
		return true;
	} else if(r = regex("--text-spacing=(.*)", arg)) {
		text_spacing = parse_value<signed>(r[1]);
		return true;
	} else if(r = regex("--font-face=(.*)", arg)) {
		fontface = parse_value<long>(r[1]);
		return true;
	} else if(r = regex("--fg-color=(.*),(.*),(.*),(.*)", arg)) {
		unsigned R = parse_value<unsigned>(r[1]);
		unsigned g = parse_value<unsigned>(r[2]);
		unsigned b = parse_value<unsigned>(r[3]);
		unsigned a = parse_value<unsigned>(r[4]);
		fg_color = (a << 24) | (b << 16) | (g << 8) | R;
		return true;
	} else if(r = regex("--fg-color=(.*),(.*),(.*)", arg)) {
		unsigned R = parse_value<unsigned>(r[1]);
		unsigned g = parse_value<unsigned>(r[2]);
		unsigned b = parse_value<unsigned>(r[3]);
		fg_color = (fg_color & 0xFF000000U) | (b << 16) | (g << 8) | R;
		return true;
	} else if(r = regex("--fg-alpha=(.*)", arg)) {
		unsigned a = parse_value<unsigned>(r[1]);
		fg_color = (fg_color & 0xFFFFFFU) | (a << 24);
		return true;
	} else if(r = regex("--bg-color=(.*),(.*),(.*),(.*)", arg)) {
		unsigned R = parse_value<unsigned>(r[1]);
		unsigned g = parse_value<unsigned>(r[2]);
		unsigned b = parse_value<unsigned>(r[3]);
		unsigned a = parse_value<unsigned>(r[4]);
		bg_color = (a << 24) | (b << 16) | (g << 8) | R;
		return true;
	} else if(r = regex("--bg-color=(.*),(.*),(.*)", arg)) {
		unsigned R = parse_value<unsigned>(r[1]);
		unsigned g = parse_value<unsigned>(r[2]);
		unsigned b = parse_value<unsigned>(r[3]);
		bg_color = (bg_color & 0xFF000000U) | (b << 16) | (g << 8) | R;
		return true;
	} else if(r = regex("--bg-alpha=(.*)", arg)) {
		unsigned a = parse_value<unsigned>(r[1]);
		bg_color = (bg_color & 0xFFFFFFU) | (a << 24);
		return true;
	} else if(r = regex("--halo-color=(.*),(.*),(.*),(.*)", arg)) {
		unsigned R = parse_value<unsigned>(r[1]);
		unsigned g = parse_value<unsigned>(r[2]);
		unsigned b = parse_value<unsigned>(r[3]);
		unsigned a = parse_value<unsigned>(r[4]);
		halo_color = (a << 24) | (b << 16) | (g << 8) | R;
		return true;
	} else if(r = regex("--halo-color=(.*),(.*),(.*)", arg)) {
		unsigned R = parse_value<unsigned>(r[1]);
		unsigned g = parse_value<unsigned>(r[2]);
		unsigned b = parse_value<unsigned>(r[3]);
		halo_color = (halo_color & 0xFF000000U) | (b << 16) | (g << 8) | R;
		return true;
	} else if(r = regex("--halo-alpha=(.*)", arg)) {
		unsigned a = parse_value<unsigned>(r[1]);
		halo_color = (halo_color & 0xFFFFFFU) | (a << 24);
		return true;
	} else if(r = regex("--halo-thickness=(.*)", arg)) {
		halo_thickness = parse_value<unsigned>(r[1]);
		if(halo_thickness < 0)
			halo_thickness = 0;
		return true;
	} else
		return false;
}

pre_subtitle::pre_subtitle()
{
	image = NULL;
}

pre_subtitle::~pre_subtitle()
{
	delete[] image;
}

subtitle::subtitle(const pre_subtitle& presub, const yuv4mpeg_stream_header& strmh)
{
	double fps = 25;
	if(strmh.fps_n && strmh.fps_d)
		fps = 1.0 * strmh.fps_n / strmh.fps_d;
	frame = presub.start.get_frame(fps, 0);
	duration = presub.duration.get_frame(fps, 0);
	width = presub.image->width;
	height = presub.image->height;
	x = position_subtitle(strmh.width, width, presub.xpos, presub.xabsolute);
	y = position_subtitle(strmh.height, height, presub.ypos, presub.yabsolute);
	if(x < 0 || y < 0 || x + width > strmh.width || y + height > strmh.height)
		std::cerr << "Warning: Subtitle is partially offscreen" << std::endl;

	if(strmh.chroma == "rgb") {
		bits16 = false;
		data = new uint8_t[width * height * 3];
		planes = 1;
		planesep = width * height * 3;
		for(size_t i = 0; i < width * height; i++) {
			data[3 * i + 0] = presub.image->data[i];
			data[3 * i + 1] = presub.image->data[i] >> 8;
			data[3 * i + 2] = presub.image->data[i] >> 16;
		}
	} else if(strmh.chroma == "444" || strmh.chroma == "444p16") {
		std::vector<uint8_t> tmp;
		bits16 = (strmh.chroma == "444p16");
		tmp.resize(width * height * 3);
		data = new uint8_t[width * height * (bits16 ? 6 : 3)];
		uint16_t* data16 = reinterpret_cast<uint16_t*>(data);
		planes = 3;
		planesep = width * height;
		yuvc_converter* conv = get_converter(strmh.find_extension("yuvmatrix=(.*)"));
		for(size_t i = 0; i < width * height; i++) {
			tmp[3 * i + 0] = presub.image->data[i];
			tmp[3 * i + 1] = presub.image->data[i] >> 8;
			tmp[3 * i + 2] = presub.image->data[i] >> 16;
		}
		if(bits16)
			conv->transform(data16, &tmp[0], width * height);
		else
			conv->transform(data, &tmp[0], width * height);
	} else
		(stringfmt() << "Chroma type " << strmh.chroma << " not supported for subtitling").throwex();
	alpha = new uint32_t[width * height];
	if(bits16)
		for(size_t i = 0; i < width * height; i++)
			alpha[i] = ((presub.image->data[i] >> 24) + (presub.image->data[i] >> 31)) << 8;
	else
		for(size_t i = 0; i < width * height; i++)
			alpha[i] = (presub.image->data[i] >> 24) + (presub.image->data[i] >> 31);
}

subtitle::~subtitle()
{
	delete[] data;
	delete[] alpha;
}


subtitle_render_context::subtitle_render_context()
{
	xpos = 0;
	ypos = -1000;
	xabsolute = false;
	yabsolute = false;
	duration = timemarker("5.0");
}

bool subtitle_render_context::argument(const std::string& arg, pre_subtitle*& presub)
{
	regex_results r;
	if(tparams.argument(arg))
		return true;
	if(r = regex("--(text|file|png)=([^,]*),(.*)", arg)) {
		std::string text = r[3];
		timemarker start(r[2]);
		parsed_png* png;
		if(r[1] == "file")
			text = read_subtitle_from_file(text);
		if(r[1] == "png")
			png = new parsed_png(text);
		else
			png = &tparams.render(text);
		presub = new pre_subtitle;
		presub->image = png;
		presub->xpos = xpos;
		presub->ypos = ypos;
		presub->xabsolute = xabsolute;
		presub->yabsolute = yabsolute;
		presub->start = start;
		presub->duration = duration;
		return true;
	} else if(r = regex("--duration=(.*)", arg)) {
		duration = timemarker(r[1]);
		return true;
	} else if(r = regex("--xpos=left", arg)) {
		xabsolute = false;
		xpos = -1000;
		return true;
	} else if(r = regex("--xpos=center", arg)) {
		xabsolute = false;
		xpos = 0;
		return true;
	} else if(r = regex("--xpos=right", arg)) {
		xabsolute = false;
		xpos = 1000;
		return true;
	} else if(r = regex("--xpos=abs:([+-]?[0-9]+)", arg)) {
		xabsolute = true;
		xpos = parse_value<ssize_t>(r[1]);
		return true;
	} else if(r = regex("--xpos=([+-]?[0-9]+)", arg)) {
		xabsolute = false;
		xpos = parse_value<ssize_t>(r[1]);
		return true;
	} else if(r = regex("--ypos=top", arg)) {
		yabsolute = false;
		ypos = -1000;
		return true;
	} else if(r = regex("--ypos=center", arg)) {
		yabsolute = false;
		ypos = 0;
		return true;
	} else if(r = regex("--ypos=bottom", arg)) {
		yabsolute = false;
		ypos = 1000;
		return true;
	} else if(r = regex("--ypos=abs:([+-]?[0-9]+)", arg)) {
		yabsolute = true;
		ypos = parse_value<ssize_t>(r[1]);
		return true;
	} else if(r = regex("--ypos=([+-]?[0-9]+)", arg)) {
		yabsolute = false;
		ypos = parse_value<ssize_t>(r[1]);
		return true;
	} else {
		return false;
	}
}

std::vector<subtitle*> subtitle::from_presub(const std::vector<pre_subtitle*>& presubs,
	const yuv4mpeg_stream_header& strmh)
{
	std::vector<subtitle*> ret;
	for(auto i : presubs) {
		try {
			ret.push_back(new subtitle(*i, strmh));
		} catch(...) {
			for(auto j : ret)
				delete j;
		}
	}
	return ret;
}

void subtitle::stamp(uint8_t* idata, size_t iwidth, size_t iheight, uint64_t frameno) const
{
	if(frameno < frame || frameno > frame + duration)
		return;
	size_t fpos_x, spos_x, swidth;
	size_t fpos_y, spos_y, sheight;
	stamp_pos(iwidth, width, x, fpos_x, spos_x, swidth);
	stamp_pos(iheight, height, y, fpos_y, spos_y, sheight);
	if(!swidth || !sheight)
		return;
	if(planes == 3 && bits16) {
		size_t iplanesep = iwidth * iheight;
		uint16_t* idata2 = reinterpret_cast<uint16_t*>(idata);
		const uint16_t* data2 = reinterpret_cast<const uint16_t*>(data);
		for(size_t i = 0; i < sheight; i++)
			for(size_t j = 0; j < swidth; j++) {
				size_t ioffset = (fpos_y + i) * iwidth + (fpos_x + j);
				size_t offset = (spos_y + i) * width + (spos_x + j);
				mix(idata2[ioffset], data2[offset], alpha[offset]);
				mix(idata2[ioffset + iplanesep], data2[offset + planesep], alpha[offset]);
				mix(idata2[ioffset + iplanesep], data2[offset + planesep], alpha[offset]);
			}
	} else if(planes == 3 && !bits16) {
		size_t iplanesep = iwidth * iheight;
		for(size_t i = 0; i < sheight; i++)
			for(size_t j = 0; j < swidth; j++) {
				size_t ioffset = (fpos_y + i) * iwidth + (fpos_x + j);
				size_t offset = (spos_y + i) * width + (spos_x + j);
				mix(idata[ioffset], data[offset], alpha[offset]);
				mix(idata[ioffset + iplanesep], data[offset + planesep], alpha[offset]);
				mix(idata[ioffset + 2 * iplanesep], data[offset + 2 * planesep], alpha[offset]);
			}
	} else if(planes == 1) {
		for(size_t i = 0; i < sheight; i++)
			for(size_t j = 0; j < swidth; j++) {
				size_t ioffset = (fpos_y + i) * 3 * iwidth + 3 * (fpos_x + j);
				size_t offset2 = (spos_y + i) * width + (spos_x + j);
				size_t offset = offset2 * 3;
				mix(idata[ioffset], data[offset], alpha[offset2]);
				mix(idata[ioffset + 1], data[offset + 1], alpha[offset2]);
				mix(idata[ioffset + 2], data[offset + 2], alpha[offset2]);
			}
	}
}
