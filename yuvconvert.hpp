#ifndef _yuvconvert_hpp_included_
#define _yuvconvert_hpp_included_

#include <cstdlib>
#include <string>

enum yuvc_rgbtype
{
	RGB_RGB,
	RGB_BGR,
	RGB_RGBX,
	RGB_BGRX,
	RGB_XRGB,
	RGB_XBGR,
};

enum yuvc_csptype
{
	CSP_PC601,
	CSP_PC709,
	CSP_REC601,
	CSP_REC709,
	CSP_PC2020,
	CSP_REC2020
};

struct yuvc_converter
{
	virtual void Y(unsigned short* out, const unsigned char* in, size_t pixels) = 0;
	virtual void Cb(unsigned short* out, const unsigned char* in, size_t pixels) = 0;
	virtual void Cr(unsigned short* out, const unsigned char* in, size_t pixels) = 0;
	void transform(unsigned short* out, const unsigned char* in, size_t pixels)
	{
		Y(out, in, pixels);
		Cb(out + pixels, in, pixels);
		Cr(out + 2 * pixels, in, pixels);
	}
	virtual void Y(unsigned char* out, const unsigned char* in, size_t pixels) = 0;
	virtual void Cb(unsigned char* out, const unsigned char* in, size_t pixels) = 0;
	virtual void Cr(unsigned char* out, const unsigned char* in, size_t pixels) = 0;
	void transform(unsigned char* out, const unsigned char* in, size_t pixels)
	{
		Y(out, in, pixels);
		Cb(out + pixels, in, pixels);
		Cr(out + 2 * pixels, in, pixels);
	}
};

yuvc_converter* yuvc_get_converter(yuvc_rgbtype type, yuvc_csptype csp);
yuvc_converter* yuvc_get_converter(yuvc_rgbtype type, const std::string& csp);
template<typename outv, typename inv>
void yuvc_shrink_croma(outv* out, const inv* in, size_t width, size_t height, bool gauss);
void yuvc_magify_luma(unsigned char* out, const unsigned char* in, size_t width, size_t height);

#endif
