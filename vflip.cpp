#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "marker.hpp"
#include "rendertext.hpp"
#include <iostream>

void swap_buffers(char* a, char* b, size_t s)
{
	for(size_t i = 0; i < s; i++)
		std::swap(a[i], b[i]);
}

void do_flip(char* content, size_t fsize, size_t height, unsigned mode)
{
	if(mode == 0) {
		//Mode 0: Flip RGB or greyscale.
		size_t lsize = fsize / height;
		for(size_t i = 0; i < height / 2; i++)
			swap_buffers(content + lsize * i, content + lsize * (height - i - 1), lsize);
	} else if(mode == 1) {
		//Mode 1: Flip YUV420.
		size_t psize = fsize * 2 / 3;
		do_flip(content, psize, height, 0);
		do_flip(content + psize, psize >> 2, height >> 1, 0);
		do_flip(content + psize + (psize >> 2), psize >> 2, height >> 1, 0);
	} else if(mode == 2) {
		//Mode 2: Flip YUV422.
		size_t psize = fsize / 2;
		do_flip(content, psize, height, 0);
		do_flip(content + psize, psize >> 1, height >> 1, 0);
		do_flip(content + psize + (psize >> 1), psize >> 1, height >> 1, 0);
	} else if(mode == 3) {
		//Mode 3: Flip YUV444.
		size_t psize = fsize / 3;
		do_flip(content, psize, height, 0);
		do_flip(content + psize, psize, height, 0);
		do_flip(content + psize + psize, psize, height, 0);
	}
}

int main(int argc, char** argv)
{
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		try {
			std::cerr << "vflip: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		} catch(std::exception& e) {
			std::cerr << "vflip: Error in option '" << arg << "': " << e.what() << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		unsigned mode;
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb") {
			framesize = 3 * strmh.width * strmh.height; mode = 0;
		} else if(strmh.chroma == "420") {
			framesize = 3 * strmh.width * strmh.height / 2; mode = 1;
		} else if(strmh.chroma == "420p16") {
			framesize = 6 * strmh.width * strmh.height / 2; mode = 1;
		} else if(strmh.chroma == "422") {
			framesize = 2 * strmh.width * strmh.height; mode = 2;
		} else if(strmh.chroma == "422p16") {
			framesize = 4 * strmh.width * strmh.height; mode = 2;
		} else if(strmh.chroma == "444") {
			framesize = 3 * strmh.width * strmh.height; mode = 3;
		} else if(strmh.chroma == "444p16") {
			framesize = 6 * strmh.width * strmh.height; mode = 3;
		} else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		write_or_die(out, static_cast<std::string>(strmh));

		std::string _framh, _framh2;
		std::vector<char> buffer;
		buffer.resize(framesize);
		struct yuv4mpeg_frame_header framh;

		//Frame loop.
		while(true) {
			if(!read_line2(in, _framh))
				return 0;	//End.
			framh = yuv4mpeg_frame_header(_framh);
			read_or_die(in, &buffer[0], buffer.size());
			do_flip(&buffer[0], framesize, strmh.height, mode);
			write_or_die(out, static_cast<std::string>(framh));
			write_or_die(out, &buffer[0], buffer.size());
		}
	} catch(std::exception& e) {
		std::cerr << "vflip: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
