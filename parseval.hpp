#ifndef _parseval_hpp_included_
#define _parseval_hpp_included_

#include <stdexcept>
#include <boost/lexical_cast.hpp>
#include <string>
#include <vector>
#include <cstdint>

template<typename T> inline T parse_value(const std::string& value) throw(std::bad_alloc, std::runtime_error)
{
	try {
		//Hack, since lexical_cast lets negative values slip through.
		if(!std::numeric_limits<T>::is_signed && value.length() && value[0] == '-') {
			throw std::runtime_error("Unsigned values can't be negative");
		}
		return boost::lexical_cast<T>(value);
	} catch(std::exception& e) {
		throw std::runtime_error("Can't parse value '" + value + "': " + e.what());
	}
}

template<> inline uint8_t parse_value(const std::string& value) throw(std::bad_alloc, std::runtime_error)
{
	try {
		//Hack, since lexical_cast doesn't handle uint8_t as value.
		int16_t v = boost::lexical_cast<int16_t>(value);
		if(v < 0 || v > 255)
			throw std::runtime_error("8-bit unsigned int out of range");
		return static_cast<uint8_t>(v);
	} catch(std::exception& e) {
		throw std::runtime_error("Can't parse value '" + value + "': " + e.what());
	}
}

template<> inline std::string parse_value(const std::string& value) throw(std::bad_alloc, std::runtime_error)
{
	return value;
}

class regex_results
{
public:
	regex_results();
	regex_results(std::vector<std::string> res);
	operator bool() const;
	bool operator!() const;
	size_t size() const;
	const std::string& operator[](size_t i) const;
private:
	bool matched;
	std::vector<std::string> results;
};

/**
 * Regexp a string and return matches.
 *
 * Parameter regex: The regexp to apply.
 * Parameter str: The string to apply the regexp to.
 * Parameter ex: If non-null and string does not match, throw this as std::runtime_error.
 * Returns: The captures.
 */
regex_results regex(const std::string& regex, const std::string& str, const char* ex = NULL)
	throw(std::bad_alloc, std::runtime_error);

/**
 * Regexp a string and return match result.
 *
 * Parameter regex: The regexp to apply.
 * Parameter str: The string to apply the regexp to.
 * Returns: True if matches, false if not.
 */
bool regex_match(const std::string& regex, const std::string& str) throw(std::bad_alloc, std::runtime_error);

/**
 * Cast string to bool.
 *
 * The following is true: 'on', 'true', 'yes', '1', 'enable', 'enabled'.
 * The following is false: 'off', 'false', 'no', '0', 'disable', 'disabled'.
 * Parameter str: The string to cast.
 * Returns: -1 if string is bad, 0 if false, 1 if true.
 */
int string_to_bool(const std::string& cast_to_bool);

int32_t utf8_parse_byte(int ch, uint16_t& state) throw();
std::u32string to_u32string(const std::string& utf8);
std::string to_u8string(const std::u32string& utf32);
size_t utf8_strlen(const std::string& str) throw();

/**
 * String formatter
 */
class stringfmt
{
public:
	stringfmt() {}
	std::string str() { return x.str(); }
	std::u32string str32() { return to_u32string(x.str()); }
	template<typename T> stringfmt& operator<<(const T& y) { x << y; return *this; }
	void throwex() { throw std::runtime_error(x.str()); }
private:
	std::ostringstream x;
};

extern const uint16_t utf8_initial_state;

/**
 * Iterator copy from UTF-8 to UTF-32
 */
template<typename srcitr, typename dstitr>
inline void copy_from_utf8(srcitr begin, srcitr end, dstitr target)
{
	uint16_t state = utf8_initial_state;
	for(srcitr i = begin; i != end; i++) {
		int32_t x = utf8_parse_byte((unsigned char)*i, state);
		if(x >= 0) {
			*target = x;
			++target;
		}
	}
	int32_t x = utf8_parse_byte(-1, state);
	if(x >= 0) {
		*target = x;
		++target;
	}
}


#endif
