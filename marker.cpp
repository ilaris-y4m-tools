#include "marker.hpp"
#include "parseval.hpp"

timemarker::timemarker()
{
	mode = 0;
}

timemarker::timemarker(const std::string& tspec)
{
	regex_results r;
	if(r = regex("(0|[1-9][0-9]*)", tspec)) {
		mode = 1;
		frame = parse_value<uint64_t>(r[1]);
	} else if(r = regex("(0|[1-9][0-9]*):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])(\\.([0-9]+))?", tspec)) {
		mode = 2;
		timepos = parse_value<uint64_t>(r[1]) * 3600 +
			parse_value<uint64_t>(r[2]) * 60 + 
			parse_value<uint64_t>(r[3]);
		if(r[4] != "")
			timepos += parse_value<double>("0" + r[4]);
	} else if(r = regex("([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])(\\.([0-9]+))?", tspec)) {
		mode = 2;
		timepos = parse_value<uint64_t>(r[1]) * 60 + 
			parse_value<uint64_t>(r[2]);
		if(r[3] != "")
			timepos += parse_value<double>("0" + r[3]);
	} else if(r = regex("(0|[1-9][0-9]*)(\\.([0-9]+))?", tspec)) {
		mode = 2;
		timepos = parse_value<uint64_t>(r[1]);
		if(r[2] != "")
			timepos += parse_value<double>("0" + r[2]);
	} else
		throw std::runtime_error("Bad timespec");
}

uint64_t timemarker::get_frame(double fps, uint64_t dflt) const
{
	if(mode == 0)
		return dflt;
	if(mode == 1)
		return frame;
	if(mode == 2)
		return fps * timepos + 0.5;
	return dflt;
}
