#include "yuv4mpeg.hpp"
#include "yuvconvert.hpp"
#include "parseval.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	enum yuvc_csptype _csp = CSP_REC601;
	std::string cspstr = "rec601";
	bool bits8 = false;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--matrix=(.+)", arg)) {
			if(r[1] == "pc601")
				_csp = CSP_PC601;
			else if(r[1] == "rec601")
				_csp = CSP_REC601;
			else if(r[1] == "pc709")
				_csp = CSP_PC709;
			else if(r[1] == "rec709")
				_csp = CSP_REC709;
			else if(r[1] == "pc2020")
				_csp = CSP_PC2020;
			else if(r[1] == "rec2020")
				_csp = CSP_REC2020;
			else {
				std::cerr << "rgbtoyuv: Unrecognized matrix '" << r[1] << "'" << std::endl;
				return 1;
			}
			cspstr = r[1];
		} else if(r = regex("--8bit", arg)) {
			bits8 = true;
		} else {
			std::cerr << "rgbtoyuv: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	yuvc_converter* converter = yuvc_get_converter(RGB_RGB, _csp);

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);

	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma != "rgb")
			throw std::runtime_error("Unsupported input chroma type (need rgb)");
		if(bits8)
			strmh.chroma = "444";
		else
			strmh.chroma = "444p16";
		strmh.extension.push_back("yuvmatrix=" + cspstr);
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		std::vector<char> buffer;
		std::vector<char> buffer3;
		buffer.resize(3 * strmh.width * strmh.height);
		if(bits8)
			buffer3.resize(3 * strmh.width * strmh.height);
		else
			buffer3.resize(6 * strmh.width * strmh.height);
		//Write frames.
		while(true) {
			std::string _framh;
			if(!read_line2(in, _framh))
				break;
			read_or_die(in, &buffer[0], buffer.size());
			struct yuv4mpeg_frame_header framh(_framh);
			if(bits8)
				converter->transform(reinterpret_cast<uint8_t*>(&buffer3[0]),
					reinterpret_cast<const uint8_t*>(&buffer[0]), strmh.width * strmh.height);
			else
				converter->transform(reinterpret_cast<uint16_t*>(&buffer3[0]),
					reinterpret_cast<const uint8_t*>(&buffer[0]), strmh.width * strmh.height);
			write_or_die(out, _framh);
			write_or_die(out, &buffer3[0], buffer3.size());
		}
	} catch(std::exception& e) {
		std::cerr << "rgbtoyuv: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
