all: rawtoy4m rgbtoyuv subsample dedup changefps resize hdify truncate concatenate imgload subtitle ffmpegsource fftaa assumefps redup vflip crop timeshow

CCFLAGS=-std=gnu++11

%.o: %.cpp
	g++ -g -Wall -Werror $(CCFLAGS) -c -o $@ $^ $(shell pkg-config libswscale --cflags) $(shell freetype-config --cflags)

rawtoy4m: rawtoy4m.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

rgbtoyuv: rgbtoyuv.o parseval.o yuv4mpeg.o yuvconvert.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

subsample: subsample.o parseval.o yuv4mpeg.o yuvconvert.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

dedup: dedup.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

changefps: changefps.o parseval.o yuv4mpeg.o fpschanger.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

assumefps: assumefps.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

resize: resize.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

hdify: hdify.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

timeshow: timeshow.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

truncate: truncate.o parseval.o yuv4mpeg.o marker.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

concatenate: concatenate.o parseval.o yuv4mpeg.o fpschanger.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

imgload: imgload.o parseval.o yuv4mpeg.o png.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs) -lz

subtitle: subtitle.o parseval.o yuv4mpeg.o png.o marker.o rendertext.o yuvconvert.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs) $(shell freetype-config --libs) -lz

ffmpegsource: ffmpegsource.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs) -lz -lavutil -lavcodec -lavformat

fftaa: fftaa.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex -lfftw3

redup: redup.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

vflip: vflip.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)

crop: crop.o parseval.o yuv4mpeg.o
	g++ $(CCFLAGS) -o $@ $^ -lboost_regex $(shell pkg-config libswscale --libs)
