#ifndef _png__hpp__included__
#define _png__hpp__included__

#include <string>

struct parsed_png
{
	parsed_png(size_t w, size_t h);
	parsed_png(const std::string& pngfile);
	~parsed_png();
	uint32_t* data;
	size_t width;
	size_t height;
private:
	parsed_png(const parsed_png&);
	parsed_png& operator=(const parsed_png&);
};

#endif
