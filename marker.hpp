#ifndef _marker_hpp_included_
#define _marker_hpp_included_

#include <string>
#include <cstdint>

class timemarker
{
public:
	timemarker();
	timemarker(const std::string& tspec);
	uint64_t get_frame(double fps, uint64_t dflt) const;
private:
	int mode;
	double timepos;
	uint64_t frame;
};

#endif