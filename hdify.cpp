#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include <iostream>

namespace
{
	void compute_scale_factors(size_t w, size_t h, double sar, size_t& fx, size_t& fy)
	{
		//sar is ratio between DAR and native DAR.
		fx = 2;
		fy = 2;
		while(sar <= sqrt(0.5)) {
			fy *= 2;
			sar *= 2;
		}
		while(sar > sqrt(2)) {
			fx *= 2;
			sar /= 2;
		}
		while(w * fx < 1920 && h * fy < 1080) {
			fx *= 2;
			fy *= 2;
		}
	}

	struct scale_params
	{
		size_t inwidth;
		size_t inheight;
		size_t factor_x;
		size_t factor_y;
		std::string chroma;
		void fill_csp()
		{
			size_t pfourths;
			size_t p1fourths;
			size_t pnfourths;
			outwidth = inwidth * factor_x;
			outheight = inheight * factor_y;
			bool subsample_x = false;
			bool subsample_y = false;
			if(chroma == "rgb") {
				pfourths = 12;
				p1fourths = 12;
				pnfourths = 0;
				planes = 1;
				mode = 0;
			} else if(chroma == "420") {
				pfourths = 6;
				p1fourths = 4;
				pnfourths = 1;
				planes = 3;
				subsample_x = true;
				subsample_y = true;
				mode = 1;
			} else if(chroma == "420p16") {
				pfourths = 12;
				p1fourths = 8;
				pnfourths = 2;
				planes = 3;
				subsample_x = true;
				subsample_y = true;
				mode = 2;
			} else if(chroma == "422") {
				pfourths = 8;
				p1fourths = 4;
				pnfourths = 2;
				planes = 3;
				subsample_x = true;
				mode = 1;
			} else if(chroma == "422p16") {
				pfourths = 16;
				p1fourths = 8;
				pnfourths = 4;
				planes = 3;
				subsample_x = true;
				mode = 2;
			} else if(chroma == "444") {
				pfourths = 12;
				p1fourths = 4;
				pnfourths = 4;
				planes = 3;
				mode = 1;
			} else if(chroma == "444p16") {
				pfourths = 24;
				p1fourths = 8;
				pnfourths = 8;
				planes = 3;
				mode = 2;
			} else
				throw std::runtime_error("Unsupported input chroma type '" + chroma + "'");
			insize = pfourths * inwidth * inheight / 4;
			outsize = pfourths * outwidth * outheight / 4;
			inplane1_size = p1fourths * inwidth * inheight / 4;
			outplane1_size = p1fourths * outwidth * outheight / 4;
			inplanen_size = pnfourths * inwidth * inheight / 4;
			outplanen_size = pnfourths * outwidth * outheight / 4;
			inplane1_stride = p1fourths * inwidth / 4;
			outplane1_stride = p1fourths * outwidth / 4;
			inplanen_stride = pnfourths * inwidth / 4;
			outplanen_stride = pnfourths * outwidth / 4;
			if(subsample_x && (inwidth | outwidth) & 1)
				throw std::runtime_error("420 and 422 modes must have even width");
			if(subsample_y && (inheight | outheight) & 1)
				throw std::runtime_error("420 modes must have even height");
			inplanen_width = inwidth / (subsample_x ? 2 : 1);
			inplanen_height = inheight / (subsample_y ? 2 : 1);
			outplanen_width = outwidth / (subsample_x ? 2 : 1);
			outplanen_height = outheight / (subsample_y ? 2 : 1);
		}
		int mode;
		size_t outwidth;
		size_t outheight;
		size_t insize;
		size_t outsize;
		size_t inplane1_size;
		size_t inplanen_size;
		size_t outplane1_size;
		size_t outplanen_size;
		size_t inplane1_stride;
		size_t inplanen_stride;
		size_t outplane1_stride;
		size_t outplanen_stride;
		size_t inplanen_width;
		size_t inplanen_height;
		size_t outplanen_width;
		size_t outplanen_height;
		unsigned planes;
	};

	void do_resize_rgb(char* out, const char* in, struct scale_params& s)
	{
		//TODO: Optimize this.
		for(size_t y = 0; y < s.inheight; y++) {
			for(size_t x = 0; x < s.inwidth; x++) {
				for(size_t i = 0; i < s.factor_x; i++) {
					out[3 * s.factor_x * x + 3 * i + 0] = in[3 * x + 0];
					out[3 * s.factor_x * x + 3 * i + 1] = in[3 * x + 1];
					out[3 * s.factor_x * x + 3 * i + 2] = in[3 * x + 2];
				}
			}
			for(size_t x = 1; x < s.factor_y; x++)
				memcpy(out + x * s.outplane1_stride, out, s.outplane1_stride);
			in += s.inplane1_stride;
			out += s.factor_y * s.outplane1_stride;
		}
	}

	template<typename T>
	void do_resize_Y(T* out, const T* in, struct scale_params& s)
	{
		//TODO: Optimize this.
		for(size_t y = 0; y < s.inheight; y++) {
			for(size_t x = 0; x < s.inwidth; x++)
				for(size_t i = 0; i < s.factor_x; i++)
					out[s.factor_x * x + i] = in[x];
			for(size_t x = 1; x < s.factor_y; x++)
				memcpy(out + x * s.outplane1_stride, out, s.outplane1_stride);
			in += s.inplane1_stride;
			out += s.factor_y * s.outplane1_stride;
		}
	}

	template<typename T>
	void do_resize_C(T* out, const T* in, struct scale_params& s)
	{
		//TODO: Optimize this.
		for(size_t y = 0; y < s.inplanen_height; y++) {
			for(size_t x = 0; x < s.inplanen_width; x++)
				for(size_t i = 0; i < s.factor_x; i++)
					out[s.factor_x * x + i] = in[x];
			for(size_t x = 1; x < s.factor_y; x++)
				memcpy(out + x * s.outplanen_stride, out, s.outplanen_stride);
			in += s.inplanen_stride;
			out += s.factor_y * s.outplanen_stride;
		}
	}

	void do_resize(char* out, const char* in, struct scale_params& s)
	{
		if(s.mode == 0) {
			do_resize_rgb(out, in, s);
		} else if(s.mode == 1) {
			do_resize_Y(reinterpret_cast<uint8_t*>(out), reinterpret_cast<const uint8_t*>(in), s);
			for(unsigned i = 0; i < s.planes - 1; i++)
				do_resize_C(reinterpret_cast<uint8_t*>(out + s.outplane1_size + i * s.outplanen_size),
					reinterpret_cast<const uint8_t*>(in + s.inplane1_size + i * s.inplanen_size),
					s);
		} else if(s.mode == 2) {
			do_resize_Y(reinterpret_cast<uint16_t*>(out), reinterpret_cast<const uint16_t*>(in), s);
			for(unsigned i = 0; i < s.planes - 1; i++)
				do_resize_C(reinterpret_cast<uint16_t*>(out + s.outplane1_size + i *
					s.outplanen_size), reinterpret_cast<const uint16_t*>(in + s.inplane1_size +
					i * s.inplanen_size), s);
		}
	}
}

int main(int argc, char** argv)
{
	scale_params s;
	s.inwidth = 0;
	s.inheight = 0;
	bool half = false;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		if(arg == "--half")
			half = true;
		else {
			std::cerr << "hdify: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		double sar = 1;
		struct yuv4mpeg_stream_header strmh(in);
		s.inwidth = strmh.width;
		s.inheight = strmh.height;

		if(strmh.sar_n && strmh.sar_d)
			sar = 1.0 * strmh.sar_n / strmh.sar_d;
		compute_scale_factors(s.inwidth, s.inheight, sar, s.factor_x, s.factor_y);
		if(half) {
			s.factor_x >>= 1;
			s.factor_y >>= 1;
		}

		s.chroma = strmh.chroma;
		s.fill_csp();
		strmh.width = s.outwidth;
		strmh.height = s.outheight;
		strmh.sar_n = 1;
		strmh.sar_d = 1;
		
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		std::vector<char> buffer;
		std::vector<char> buffer2;
		buffer.resize(s.insize + 128);
		buffer2.resize(s.outsize + 128);
		while(true) {
			std::string _framh;
			if(!read_line2(in, _framh))
				break;
			read_or_die(in, &buffer[0], s.insize);
			do_resize(&buffer2[0], &buffer[0], s);
			write_or_die(out, _framh);
			write_or_die(out, &buffer2[0], s.outsize);
			
		}
	} catch(std::exception& e) {
		std::cerr << "hdify: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
