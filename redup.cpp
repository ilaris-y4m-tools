#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include <iostream>

uint64_t not_valid = 0xFFFFFFFFFFFFFFFFULL;

uint64_t nscale[7] = {1000000, 100000, 10000, 1000, 100, 10, 1};

struct redup_logic
{
	redup_logic(FILE* _tc, uint32_t _fps_n, uint64_t _fps_d)
	{
		char buffer[512];
		tc = _tc;
		fps_n = _fps_n;
		fps_d = _fps_d;
		fgets(buffer, 512, tc);
		if(!regex_match(buffer, "# timecode format v2\r?\n"))
			throw std::runtime_error("Not valid TC file (header)");
		current_frame = not_valid;
		next_frame = 0;
	}
	size_t get()
	{
		current_frame = next_frame;
		next_frame = read_framenum();
		return (next_frame > current_frame) ?  (next_frame - current_frame) : 0;
	}
private:
	uint64_t read_framenum()
	{
		char buffer[512];
		if(fgets(buffer, 512, tc) && buffer[0])
			return _read_framenum(buffer);
		else
			return current_frame + 1;
	}
	uint64_t _read_framenum(const char* buf)
	{
		regex_results r = regex("([0-9]+)(\\.([0-9]+))?\r?\n", buf);
		if(!r)
			throw std::runtime_error("Not valid TC file (value)");
		uint64_t nanosec = parse_value<uint64_t>(r[1]) * 1000000ULL;
		if(r[3].length() > 6)
			nanosec += parse_value<uint64_t>(r[3].substr(0, 6));
		else if(r[3].length() > 0)
			nanosec += parse_value<uint64_t>(r[3]) * nscale[r[3].length()];
		uint64_t blocklen = fps_d * 1000000000ULL;
		uint64_t blocks = nanosec / blocklen;
		nanosec %= blocklen;
		return blocks * fps_n + (nanosec / 1000000000.0 * fps_n / fps_d);
	}
	FILE* tc;
	uint32_t fps_n;
	uint32_t fps_d;
	uint64_t current_frame;
	uint64_t next_frame;
};

int main(int argc, char** argv)
{
	uint64_t infr = 0;
	uint64_t outfr = 0;
	uint64_t lost = 0;
	uint32_t ofps_n = 25;
	uint32_t ofps_d = 1;
	uint32_t fps_n = 0;
	uint32_t fps_d = 0;
	size_t framesize = 0;
	std::string tcfile;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--fps=([1-9][0-9]*)", arg)) {
			try {
				fps_n = parse_value<unsigned>(r[1]);
				fps_d = 1;
			} catch(std::exception& e) {
				std::cerr << "redup: Bad fps '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--fps=([1-9][0-9]*)/([1-9][0-9]*)", arg)) {
			try {
				fps_n = parse_value<unsigned>(r[1]);
				fps_d = parse_value<unsigned>(r[2]);
			} catch(std::exception& e) {
				std::cerr << "redup: Bad fps '" << r[1] << "/" << r[2] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--.*", arg)) {
			std::cerr << "redup: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		} else {
			if(tcfile != "") {
				std::cerr << "redup: Only one TC file may be specified." << std::endl;
				return 1;
			}
			tcfile = arg;
		}
	}

	if(tcfile == "") {
		std::cerr << "redup: TC file must be specified." << std::endl;
		return 1;
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	FILE* tc = fopen(tcfile.c_str(), "r");
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);

	if(!tc) {
		std::cerr << "redup: Can't open TC file '" << tcfile << "'." << std::endl;
		return 1;
	}

	//Fixup header.
	try {
		regex_results r;
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "420")
			framesize = 3 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "420p16")
			framesize = 6 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "422")
			framesize = 2 * strmh.width * strmh.height;
		else if(strmh.chroma == "422p16")
			framesize = 4 * strmh.width * strmh.height;
		else if(strmh.chroma == "444")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "444p16")
			framesize = 6 * strmh.width * strmh.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		if(strmh.fps_n && strmh.fps_d) {
			ofps_n = strmh.fps_n;
			ofps_d = strmh.fps_d;
		}
		if(fps_n && fps_d) {
			strmh.fps_n = fps_n;
			strmh.fps_d = fps_d;
		} else {
			fps_n = ofps_n;
			fps_d = ofps_d;
		}
		strmh.delete_extensions("VFR");
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);


		std::string _framh, _framh2;
		std::vector<char> buffer;
		buffer.resize(framesize);
		struct yuv4mpeg_frame_header framh;
		struct redup_logic rdl(tc, fps_n, fps_d);

		size_t count = 0;

		//Initial black frame.
		count = rdl.get();
		for(size_t i = 0; i < count; i++) {
			outfr++;
			std::string _framh2 = framh;
			write_or_die(out, _framh2);
			write_or_die(out, &buffer[0], framesize);
		}

		//Frame loop.
next_input:
		if(!read_line2(in, _framh))
			goto out;	//End.
		framh = yuv4mpeg_frame_header(_framh);
		infr++;
		read_or_die(in, &buffer[0], buffer.size());

		count = rdl.get();
		framh.duration = 1;
		if(!count) lost++;
		for(size_t i = 0; i < count; i++) {
			outfr++;
			std::string _framh2 = framh;
			write_or_die(out, _framh2);
			write_or_die(out, &buffer[0], framesize);
		}
		goto next_input;
	} catch(std::exception& e) {
		std::cerr << "redup: Error: " << e.what() << std::endl;
		return 1;
	}
out:
	std::cerr << "redup: Read " << infr << " frames, wrote " << outfr << " frames." << std::endl;
	std::cerr << "redup: Lost " << lost << " frames." << std::endl;
	return 0;
}
