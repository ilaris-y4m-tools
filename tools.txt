Common constructs:
------------------
<timespec>: One of:
	<integer>: Frame number.
	<decimal>: Point in seconds.
	[<h>:]<m>:<s>[.<subsec>]: Point decomposed to hours, minutes and seconds.
<fps>: One of:
	<integer>: Integer fps.
	<integer>/<integer>: Fractional fps.

assumefps:
==========
Change the fps in stream header without changing the actual stream.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--fps=<fps>
	The new framerate to apply.

changefps:
==========
Change the fps of stream. Frames are duplicated and dropped. VFR streams become CFR.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--fps=<fps>
	The new framerate to apply.

concatenate:
============
Output concatenation of multiple streams. Stream parameters (except framerate) must match.

If framerates do not match, then the non-matching ones are converted to match.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--primary-header=<num>
	Set from which stream to copy the stream header (first is 0, next is 1, and so on).
<filename>
	Name of file to copy. Use '-' to copy standard input (may be done only once). At least one is required.

crop:
=====
Crop a stream.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
<x>,<y>,<w>,<h>
	Crop region of size <w>,<h> starting from <x>,<y>.
<x>,<y>,<x2>,<y2>
	Crop rectangle <x>,<y> to <x2>,<y2>.

Notes:
------
- If stream is subsampled, offsets and sizes must be integral multiple of block size (x coordinates/sizes even
  for 422, both x and y coordinates/sizes even for 420).

dedup:
======
Discard identical frames.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--max=<number>
	Maximum number of consequtive frames to discard.

Notes:
------
VFR streams can't be dedupped.

ffmpegsource:
=============
Read file using ffmpeg.

Supported chroma: N/A (output RGB)

Options:
--------
--format=<string>
	Force format.
--sar=<num>/<num>
	Set output SAR.
--no-sync
	Do not attempt to sync video, output each frame once.
--ignore-length
	Ignore reported length of file, read until end of video.
<filename>
	File to read. Exactly one required.

Notes:
------
- Use --ignore-length for MKV files.
- The automatic syncing can create sync problems (or resolve those).

fftaa:
======
Badly broken FFT-based antialiaser. DON'T USE.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--resolution=<w>x<h>
	Set output resolution.
--scale=<x>x<y>
	Scale down by <x> horizontally and by <y> vertically.
--scale=<n>
	Scale down by <n>.

One of options is required.

Notes:
------
- This filter rings like hell and is not to be used.

hdify:
======
Point scale stream to HD resolution.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--half
	Only scale to half HD resolution.

Notes:
------
Scaling to half resolution is to be used with --double option of subsample.

imgload:
========
Load PNG image as video.

Supported chroma: N/A (output RGB)

Options:
--------
--duration=<number>
--duration=<number>/<number>
	Number of seconds image lasts. Default is 2.

rawtoy4m:
=========
Import raw RGB stream and make Y4M wrapper around it.

Supported chroma: RGB

Options:
--------
--resolution=<num>x<num>
	Resolution to use. Required.
--fps=<fps>
	Fps to use.
--progressive
	No interlacing. Default.
--interlace=top
	Interlace top field first.
--interlace=bottom
	Interlace bottom field first.

redup:
======
Apply timecodes to convert video + timecodes back to CFR.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--fps=<fps>
	Base fps to use.
<filename>
	Timecode file. Exactly one required.

Notes:
------
- The frame durations in original file are ignored. Each frame is processed according to timecodes in sequence.
- Base fps controls the fps used for snapping the frames to and is the fps of resulting stream.

resize:
=======
Resize image.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--precorrect
	Do aspect precorrection. Affects the output resolution and effects changing output SAR to 1:1.
--resolution=<num>x<num>
	Set output resolution.
--factor=/<num>
	Downscale by <num>.
--factor=x<num>
	Upscale by <num>.
--factor=/<num>x/<num>
	Downscale by <num>x<num>.
--factor=x<num>xx<num>
	Upscale by <num>x<num>.
--fast-bilinear
--bilinear
--bicubic
--experimential
--point
--area
--bicubic-linear
--gauss
--sinc
--lanczos
--spline
	Set algorithm.

If --precorrect is not specified, --resolution or --factor is required.

rgbtoyuv:
=========
Convert RGB stream to YUV444.

Supported chroma: RGB (output 444 or 444p16)

Options:
--------
--matrix=<matrix>
	Set matrix to use. Valid values are 'pc601', 'rec601', 'pc709', 'rec709'. Default is 'rec601'.
--8bit
	Output 444 instead of 444p16.


subsample:
==========
Subsample 444 to 420.

Supported chroma: 444, 444p16 (output 420 or 420p16).

Options:
--------
--16bit
	Output 420p16 instead of 420.
--bilinear
	Use bilinear downsampling for chroma.
--double
	Convert by upscaling luma 2x using point scaling and leaving chroma alone.

subtitle:
=========
Add subtitles.

Supported chroma: RGB, 444, 444p16

Options:
--------
--font-file=<file>
	Set font file to use. Required for text rendering.
--font-size=<int>
	Set font size.
--text_alignment=left
--text_alignment=center
--text_alignment=right
	Set inter-line text alignment.
--text_alignment=<num>
	Set text alignment on range -1000...1000.
--text-spacing=<num>
	Set number padding pixels betweeen rows.
--font-face=<num>
	Set font face withing font file.
--fg-color=<r>,<g>,<b>[,<a>]
	Set font foreground color. Alpha range is 0-256.
--bg-color=<r>,<g>,<b>[,<a>]
	Set font background color. Alpha range is 0-256.
--halo-color=<r>,<g>,<b>[,<a>]
	Set font halo color. Alpha range is 0-256.
--fg-alpha=<num>
	Set font foreground alpha (0-256).
--bg-alpha=<num>
	Set font background alpha (0-256).
--halo-alpha=<num>
	Set font halo alpha (0-256).
--halo-thickness=<num>
	Set font halo thickness.
--text=<timespec>,<text>
	Render text starting at given time. '\' is escaped as '\\'. Linefeed is escaped as '\n'.
--file=<timespec>,<filename>
	Read raw text to render from <filename>.
--png=<timespec>,<filename>
	Read PNG image to render from specified file. Text rendering options have no effect.
--duration=<timespec>
	Set duration of subtitle.
--xpos=left
--xpos=center
--xpos=right
--xpos=abs:<coordinate>
--xpos=<relative>
	Set x-position of the subtitle. The last form has -1000 be the same as left and 1000 the same as right.
--ypos=top
--ypos=center
--ypos=bottom
--ypos=abs:<coordinate>
--ypos=<relative>
	Set y-position of the subtitle. The last form has -1000 be the same as top and 1000 the same as bottom.

Notes:
------
- If applied to 444 stream, converts RGB values to YUV according to the matrix used (rec601 if unknown).

timeshow:
=========
Show progress.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--interval=<seconds>
	Set number of seconds between showing progress messages.

truncate:
=========
Truncate streams.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16

Options:
--------
--start=<timespec>
	Starting point to cut.
--end=<timespec>
	Ending point to cut.

vflip:
======
Flip the image vertically.

Supported chroma: RGB, 420, 420p16, 422, 422p16, 444, 444p16
