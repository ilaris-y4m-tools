#include "png.hpp"
#include "parseval.hpp"
#include <iomanip>
#include <zlib.h>
#include <stdexcept>
#include <cstring>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>

#define BLOCK 8192
#define MAXBLOCK 8192

namespace
{
	const unsigned stype_1bit = 0;
	const unsigned stype_2bit = 1;
	const unsigned stype_4bit = 2;
	const unsigned stype_8bit = 3;
	const unsigned stype_mask = 3;
	const unsigned pstride_s = 0;
	const unsigned pstride_2 = 4;
	const unsigned pstride_3 = 8;
	const unsigned pstride_4 = 12;
	const unsigned pstride_mask = 12;
	const unsigned nopalette = 0;
	const unsigned paletted = 16;
	const unsigned greyscale_trans = 32;
	const unsigned rgb_trans = 64;

	size_t bulk_read(std::istream& stream, void* buffer, size_t size)
	{
		size_t done = 0;
		char* tmp = reinterpret_cast<char*>(buffer);
		while(size > 0) {
			size_t bsize = size;
			if(bsize > MAXBLOCK)
				bsize = MAXBLOCK;
			stream.read(tmp, bsize);
			size_t r = stream.gcount();
			done += r;
			tmp += r;
			size -= r;
			if(!stream)
				return done;
		}
		return done;
	}

	void load_file(std::vector<uint8_t>& data, std::istream& in, const char* edata, size_t edatasize)
	{
		data.resize(edatasize);
		for(size_t i = 0; i < edatasize; i++)
			data[i] = edata[i];
		while(true) {
			size_t pos = data.size();
			data.resize(pos + BLOCK);
			size_t r = bulk_read(in, &data[pos], BLOCK);
			if(r < BLOCK) {
				data.resize(pos + r);
				return;
			}
		}
	}

	uint32_t read32(const uint8_t* p)
	{
		return ((uint32_t)p[0] << 24) | ((uint32_t)p[1] << 16) | ((uint32_t)p[2] << 8) | (uint32_t)p[3];
	}

	void* zlib_alloc(void* dummy, uInt s1, uInt s2)
	{
		return malloc(static_cast<size_t>(s1) * s2);
	}

	void zlib_free(void* dummy, void* addr)
	{
		free(addr);
	}

	int throw_zlib_error(int err)
	{
		if(err == Z_ERRNO) {
			int _err = errno;
			throw std::runtime_error(std::string("OS error: ") + strerror(_err));
		}
		if(err == Z_STREAM_ERROR)
			throw std::runtime_error("Stream error");
		if(err == Z_DATA_ERROR)
			throw std::runtime_error("Compressed data corrupt");
		if(err == Z_MEM_ERROR)
			throw std::bad_alloc();
		if(err == Z_BUF_ERROR)
			throw std::runtime_error("Buffer error");
		if(err == Z_VERSION_ERROR)
			throw std::runtime_error("Zlib version incompatiblity");
		if(err < 0) {
			std::ostringstream x;
			x << "Unknown zlib error #" << -err << std::endl;
			throw std::runtime_error(x.str());
		}
		return err;
	}

	struct png_header
	{
		png_header(const uint8_t* buffer, size_t bufsize, size_t& nextptr);
	};
	
	struct png_chunk
	{
		png_chunk(const uint8_t* buffer, size_t bufsize, size_t chunkptr, size_t& nextptr);
		uint32_t type;
		const uint8_t* data;
		size_t size;
	};

	class png_idat_decoder
	{
	public:
		png_idat_decoder(std::vector<png_chunk>& _chunks);
		~png_idat_decoder();
		size_t decode(uint8_t* buffer, size_t maxbytes);
	private:
		png_idat_decoder(png_idat_decoder&);
		png_idat_decoder& operator=(png_idat_decoder&);
		std::vector<png_chunk>& chunks;
		z_stream inflater;
		size_t block_num;
		size_t block_pointer;
		bool eof;
	};


	png_header::png_header(const uint8_t* buffer, size_t bufsize, size_t& nextptr)
	{
		if(bufsize < 8 || memcmp(buffer, "\x89PNG\r\n\x1A\n", 8))
			throw std::runtime_error("Wrong PNG magic");
		nextptr = 8;
	}

	png_chunk::png_chunk(const uint8_t* buffer, size_t bufsize, size_t chunkptr, size_t& nextptr)
	{
		if(chunkptr > bufsize - 12)
			throw std::runtime_error("Incomplete PNG chunk");
		size = read32(buffer + chunkptr);
		if(chunkptr > bufsize - 12 - size)
			throw std::runtime_error("Incomplete PNG chunk");
		uint32_t alleged_crc = read32(buffer + chunkptr + size + 8);
		uint32_t actual_crc = crc32(crc32(0, NULL, 0), buffer + chunkptr + 4, size + 4);
		if(alleged_crc != actual_crc)
			throw std::runtime_error("PNG chunk CRC mismatch");
		data = buffer + chunkptr + 8;
		type = read32(buffer + chunkptr + 4);
		nextptr = chunkptr + size + 12;
	}

	png_idat_decoder::png_idat_decoder(std::vector<png_chunk>& _chunks)
		: chunks(_chunks)
	{
		memset(&inflater, 0, sizeof(inflater));
		inflater.zalloc = zlib_alloc;
		inflater.zfree = zlib_free;
		throw_zlib_error(inflateInit(&inflater));
		block_num = 0;
		//Find first IDAT.
		while(block_num < chunks.size() && chunks[block_num].type != 0x49444154)
			block_num++;
		if(block_num == chunks.size())
			throw std::runtime_error("PNG file has no IDAT chunks");
		block_pointer = 0;
		eof = false;
	}
	
	png_idat_decoder::~png_idat_decoder()
	{
		inflateEnd(&inflater);
	}
	
	size_t png_idat_decoder::decode(uint8_t* buffer, size_t maxbytes)
	{
		if(eof)
			return 0;
		size_t done = 0;
		while(maxbytes > 0) {
			unsigned char dummy;
			if(block_num < chunks.size()) {
				inflater.next_in = const_cast<uint8_t*>(chunks[block_num].data + block_pointer);
				inflater.avail_in = chunks[block_num].size - block_pointer;
			} else {
				inflater.next_in = &dummy;
				inflater.avail_in = 0;
			}
			if(inflater.avail_in < chunks[block_num].size - block_pointer) {
				//Set to maximum.
				inflater.avail_in = 0;
				inflater.avail_in--;
			}
			inflater.next_out = buffer;
			inflater.avail_out = maxbytes;
			if(inflater.avail_out < maxbytes) {
				//Set to maximum.
				inflater.avail_out = 0;
				inflater.avail_out--;
			}
			size_t orig_avail_in = inflater.avail_in;
			size_t orig_avail_out = inflater.avail_out;
			int c = throw_zlib_error(inflate(&inflater, Z_SYNC_FLUSH));
			if(c == Z_NEED_DICT)
				throw std::runtime_error("PNG decoding needs a dictionary?");
			block_pointer += (orig_avail_in - inflater.avail_in);
			buffer += (orig_avail_out - inflater.avail_out);
			done += (orig_avail_out - inflater.avail_out);
			maxbytes -= (orig_avail_out - inflater.avail_out);
			if(c == Z_STREAM_END) {
				eof = true;
				break;
			}
			if(block_num == chunks.size() && inflater.avail_out && c == Z_OK)
				throw std::runtime_error("PNG compressed data underrun");
			if(block_num < chunks.size() && block_pointer == chunks[block_num].size) {
				do {
					block_num++;
				} while(block_num < chunks.size() && chunks[block_num].type != 0x49444154);
				block_pointer = 0;
			}
		}
		return done;
	}

	void load_palette(uint32_t* palette, std::vector<png_chunk>& chunks)
	{
		bool ok = false;
		for(auto i : chunks) {
			if(i.type == 0x504C5445) {
				unsigned colors = i.size / 3;
				for(unsigned j = 0; j < colors; j++) {
					palette[j] = static_cast<uint32_t>(i.data[3 * j + 0]);
					palette[j] |= (static_cast<uint32_t>(i.data[3 * j + 1] << 8));
					palette[j] |= (static_cast<uint32_t>(i.data[3 * j + 2] << 16));
					palette[j] |= 0xFF000000U;
				}
				for(unsigned j = colors; j < 256; j++)
					palette[j] = 0xFF000000U;
				ok = true;
			} else if(i.type == 0x74524E53) {
				for(unsigned j = 0; j < i.size; j++) {
					palette[j] &= 0xFFFFFF;
					palette[j] |= (static_cast<uint32_t>(i.data[j] << 24));
				}
			}
		}
		if(!ok)
			throw std::runtime_error("PNG file is marked as indexed but has no palette");
	}

	void load_gtrans(uint32_t* palette, std::vector<png_chunk>& chunks, unsigned stype)
	{
		palette[0] = 65536;		//Invalid.
		for(auto i : chunks) {
			if(i.type == 0x74524E53) {
				if(i.size < 2)
					throw std::runtime_error("tRNS chunk too short");
				palette[0] = static_cast<uint32_t>(i.data[1]);
			}
		}
	}

	void load_ctrans(uint32_t* palette, std::vector<png_chunk>& chunks)
	{
		palette[0] = 0;		//Invalid.
		for(auto i : chunks) {
			if(i.type == 0x74524E53) {
				if(i.size < 6)
					throw std::runtime_error("tRNS chunk too short");
				palette[0] = static_cast<uint32_t>(i.data[1]);
				palette[0] |= (static_cast<uint32_t>(i.data[3] << 8));
				palette[0] |= (static_cast<uint32_t>(i.data[5] << 16));
				palette[0] |= 0xFF000000U;
			}
		}
	}

	void scanline_inverse_filter(uint8_t* data, const uint8_t* last, size_t bytes, size_t psize, uint8_t filter)
	{
		if(filter == 0)
			;
		else if(filter == 1)
			for(unsigned i = psize; i < bytes; i++)
				data[i] += data[i - psize];
		else if(filter == 2)
			for(unsigned i = 0; i < bytes; i++)
				data[i] += last[i];
		else if(filter == 3) {
			for(unsigned i = 0; i < psize; i++)
				data[i] += last[i] / 2;
			for(unsigned i = psize; i < bytes; i++)
				data[i] += ((uint16_t)last[i] + data[i - psize]) / 2;
		} else if(filter == 4) {
			for(unsigned i = 0; i < psize; i++)
				data[i] += last[i];
			for(unsigned i = psize; i < bytes; i++) {
				int16_t p = (int16_t)last[i] + data[i - psize] - last[i - psize];
				uint16_t pa = abs(p - data[i - psize]);
				uint16_t pb = abs(p - last[i]);
				uint16_t pc = abs(p - last[i - psize]);
				if(pa <= pb && pa <= pc)
					data[i] += data[i - psize];
				else if(pb <= pc)
					data[i] += last[i];
				else
					data[i] += last[i - psize];
			}
		} else
			throw std::runtime_error("Unsupported scanline filter");
	}

	template<unsigned stype>
	uint32_t grey_expand(uint8_t val)
	{
		if(stype == stype_1bit) return (val & 1) * 0xFFFFFF;
		if(stype == stype_2bit) return (val & 3) * 0x555555;
		if(stype == stype_4bit) return (val & 15) * 0x111111;
		if(stype == stype_8bit) return val * 0x010101;
	}

	template<unsigned stype, bool grey>
	void _decode_palette(parsed_png& adata, png_idat_decoder& idata, uint32_t* palette)
	{
		uint32_t gtrans = palette[0];
		size_t scan_bytes;
		if(stype == stype_1bit) scan_bytes = (adata.width + 7) / 8;
		if(stype == stype_2bit) scan_bytes = (adata.width + 3) / 4;
		if(stype == stype_4bit) scan_bytes = (adata.width + 1) / 2;
		if(stype == stype_8bit) scan_bytes = (adata.width + 0) / 1;
		std::vector<uint8_t> prevline, thisline;
		prevline.resize(scan_bytes);
		thisline.resize(scan_bytes + 1);
		adata.data = new uint32_t[adata.width * adata.height];
		memset(&prevline[0], 0, scan_bytes);

		for(size_t i = 0; i < adata.height; i++) {
			size_t r = idata.decode(&thisline[0], scan_bytes + 1);
			if(r < scan_bytes + 1)
				throw std::runtime_error("PNG uncompressed data underrun");
			scanline_inverse_filter(&thisline[1], &prevline[0], scan_bytes, 1, thisline[0]);
			uint32_t* dest2 = adata.data + i * adata.width;
			for(size_t i = 0; i < adata.width; i++) {
				uint8_t s;
				if(stype == stype_1bit)
					s = (thisline[i / 8 + 1] >> (7 - (i % 8))) & 1;
				if(stype == stype_2bit)
					s = (thisline[i / 4 + 1] >> (6 - (i % 4) * 2)) & 3;
				if(stype == stype_4bit)
					s = (thisline[i / 2 + 1] >> (4 - (i % 2) * 4)) & 15;
				if(stype == stype_8bit)
					s = thisline[i + 1];
				if(grey) {
					dest2[i] = grey_expand<stype>(s) | ((s != gtrans) ? 0xFF000000U : 0);
				} else
					dest2[i] = palette[s];
			}
			memcpy(&prevline[0], &thisline[1], scan_bytes);
		}
	}

	template<bool grey>
	void decode_palette(parsed_png& adata, png_idat_decoder& idata, unsigned stype, uint32_t* palette)
	{
		if(stype == stype_1bit) _decode_palette<stype_1bit, grey>(adata, idata, palette);
		if(stype == stype_2bit) _decode_palette<stype_2bit, grey>(adata, idata, palette);
		if(stype == stype_4bit) _decode_palette<stype_4bit, grey>(adata, idata, palette);
		if(stype == stype_8bit) _decode_palette<stype_8bit, grey>(adata, idata, palette);
	}

	template<unsigned ppitch>
	void decode_rgb(parsed_png& adata, png_idat_decoder& idata, uint32_t ckey)
	{
		size_t scan_bytes = ppitch * adata.width;
		std::vector<uint8_t> prevline, thisline;
		prevline.resize(scan_bytes);
		thisline.resize(scan_bytes + 1);
		adata.data = new uint32_t[adata.width * adata.height];
		memset(&prevline[0], 0, scan_bytes);
	
		for(size_t i = 0; i < adata.height; i++) {
			size_t r = idata.decode(&thisline[0], scan_bytes + 1);
			if(r < scan_bytes + 1)
				throw std::runtime_error("PNG uncompressed data underrun");
			scanline_inverse_filter(&thisline[1], &prevline[0], scan_bytes, ppitch, thisline[0]);
			uint32_t* dest = adata.data + i * adata.width;
			for(size_t i = 0; i < adata.width; i++) {
				if(ppitch == 2) {
					dest[i] = static_cast<uint32_t>(thisline[ppitch * i + 1]);
					dest[i] |= (static_cast<uint32_t>(thisline[ppitch * i + 1]) << 8);
					dest[i] |= (static_cast<uint32_t>(thisline[ppitch * i + 1]) << 16);
					dest[i] |= (static_cast<uint32_t>(thisline[ppitch * i + 2]) << 24);
				} else {
					dest[i] = static_cast<uint32_t>(thisline[ppitch * i + 1]);
					dest[i] |= (static_cast<uint32_t>(thisline[ppitch * i + 2]) << 8);
					dest[i] |= (static_cast<uint32_t>(thisline[ppitch * i + 3]) << 16);
					if(ppitch > 3)
						dest[i] |= (static_cast<uint32_t>(thisline[ppitch * i + 4]) << 24);
					else {
						dest[i] |= 0xFF000000U;
						if(dest[i] == ckey)
							dest[i] &= 0xFFFFFF;
					}
				}
			}
			memcpy(&prevline[0], &thisline[1], scan_bytes);
		}
	}

	void load_png3(parsed_png& adata, std::vector<png_chunk>& chunks, unsigned ctype)
	{
		unsigned stype = ctype & stype_mask;
		unsigned pstride = ctype & pstride_mask;
		bool palette_lookup = ctype & paletted;
		bool gtrans = ctype & greyscale_trans;
		bool ctrans = ctype & rgb_trans;
		
		uint32_t palette[256] = {0};
		if(palette_lookup)
			load_palette(palette, chunks);
		else if(gtrans)
			load_gtrans(palette, chunks, stype);
		else if(ctrans)
			load_ctrans(palette, chunks);
		png_idat_decoder idat(chunks);
		if(pstride == pstride_s && palette_lookup)
			decode_palette<false>(adata, idat, stype, palette);
		if(pstride == pstride_s && !palette_lookup)
			decode_palette<true>(adata, idat, stype, palette);
		if(pstride == pstride_2)
			decode_rgb<2>(adata, idat, palette[0]);
		if(pstride == pstride_3)
			decode_rgb<3>(adata, idat, palette[0]);
		if(pstride == pstride_4)
			decode_rgb<4>(adata, idat, palette[0]);
	}

	void load_png2(parsed_png& adata, std::vector<png_chunk>& chunks)
	{
		if(chunks[0].size < 13)
			throw std::runtime_error("IHDR chunk too small");
		adata.width = read32(chunks[0].data + 0);
		adata.height = read32(chunks[0].data + 4);
		uint8_t bitdepth = chunks[0].data[8];
		uint8_t colortype = chunks[0].data[9];
		uint8_t compression = chunks[0].data[10];
		uint8_t filterbank = chunks[0].data[11];
		uint8_t interlace = chunks[0].data[11];
		uint8_t ctype;
		if(compression != 0)
			throw std::runtime_error("Unsupported compression method");
		if(filterbank != 0)
			throw std::runtime_error("Unsupported filter bank");
		if(interlace != 0)
			throw std::runtime_error("Unsupported interlace type");
		if(colortype == 0 && bitdepth == 1)
			ctype = stype_1bit | pstride_s | nopalette | greyscale_trans;
		else if(colortype == 0 && bitdepth == 2)
			ctype = stype_2bit | pstride_s | nopalette | greyscale_trans;
		else if(colortype == 0 && bitdepth == 4)
			ctype = stype_4bit | pstride_s | nopalette | greyscale_trans;
		else if(colortype == 0 && bitdepth == 8)
			ctype = stype_8bit | pstride_s | nopalette | greyscale_trans;
		else if(colortype == 2 && bitdepth == 8)
			ctype = stype_8bit | pstride_3 | nopalette | rgb_trans;
		else if(colortype == 3 && bitdepth == 1)
			ctype = stype_1bit | pstride_s | paletted;
		else if(colortype == 3 && bitdepth == 2)
			ctype = stype_2bit | pstride_s | paletted;
		else if(colortype == 3 && bitdepth == 4)
			ctype = stype_4bit | pstride_s | paletted;
		else if(colortype == 3 && bitdepth == 8)
			ctype = stype_8bit | pstride_s | paletted;
		else if(colortype == 4 && bitdepth == 8)
			ctype = stype_8bit | pstride_2 | nopalette;
		else if(colortype == 6 && bitdepth == 8)
			ctype = stype_8bit | pstride_4 | nopalette;
		else
			throw std::runtime_error("Unsupported color type or bit depth");
		load_png3(adata, chunks, ctype);
	}

	void load_png(std::istream& in, parsed_png& adata, const char* data, size_t datasize)
	{
		std::vector<uint8_t> filedata;
		load_file(filedata, in, data, datasize);
		const uint8_t* fdata = &filedata[0];
		size_t fsize = filedata.size();
		size_t iptr = 0;
		try {
			std::vector<png_chunk> chunks;
			png_header header(fdata, fsize, iptr);
			while(iptr < fsize)
				chunks.push_back(png_chunk(fdata, fsize, iptr, iptr));
			if(chunks.empty() || chunks[0].type != 0x49484452)
				throw std::runtime_error("First chunk must be IHDR");
			if(chunks.empty() || chunks[chunks.size() - 1].type != 0x49454E44)
				throw std::runtime_error("Last chunk must be IEND");
			load_png2(adata, chunks);
		} catch(std::exception& e) {
			throw std::runtime_error(std::string("Can't load PNG image: ") + e.what());
		}
	}

}

parsed_png::parsed_png(size_t w, size_t h)
{
	data = new uint32_t[w * h];
	width = w;
	height = h;
}

parsed_png::parsed_png(const std::string& pngfile)
{
	data = NULL;
	char dummy;
	std::ifstream x(pngfile);
	if(!x)
		throw std::runtime_error("Can't open PNG file");
	load_png(x, *this, &dummy, 0);
}

parsed_png::~parsed_png()
{
	if(data) delete[] data;
}

#ifdef TEST_PNG_DECODER
int main(int argc, char** argv)
{
	parsed_png p(argv[1]);
	if(p.adata) {
		std::cerr << "Parsed as RGBA, " << p.width << "*" << p.height << std::endl;
		std::ofstream y("png.dump");
		for(size_t i = 0; i < p.width * p.height; i++)
			y.write(reinterpret_cast<char*>(p.adata + i), 4);
	} else if(p.tdata) {
		std::cerr << "Parsed as RGB, " << p.width << "*" << p.height << std::endl;
		std::ofstream y("png.dump");
		for(size_t i = 0; i < p.width * p.height; i++)
			y.write(reinterpret_cast<char*>(p.tdata + i), 3);
	} else if(p.pdata) {
		std::cerr << "Parsed as indexed, " << p.width << "*" << p.height << std::endl;
		std::ofstream y("png.dump");
		for(size_t i = 0; i < p.width * p.height; i++) {
			char buf[3];
			buf[0] = buf[1] = buf[2] = p.pdata[i] ? 255 : 0;
			y.write(buf, 3);
		}
	} else {
		std::cerr << "Unknown type" << std::endl;
	}
}
#endif
