#include "fpschanger.hpp"

fpschanger::fpschanger(uint32_t _tfps_n, uint32_t _tfps_d, uint32_t sfps_n, uint32_t sfps_d)
{
	if(!_tfps_n || !_tfps_d) {
		_tfps_n = 25;
		_tfps_d = 1;
	}
	if(!sfps_n || !sfps_d) {
		sfps_n = 25;
		sfps_d = 1;
	}
	tfps_n = _tfps_n;
	tfps_d = _tfps_d;
	increment = static_cast<uint64_t>(tfps_n) * sfps_d;
	decrement = static_cast<uint64_t>(tfps_d) * sfps_n; 
	balance = 0;
}

void fpschanger::change_source_rate(uint32_t sfps_n, uint32_t sfps_d)
{
	if(!sfps_n || !sfps_d) {
		sfps_n = 25;
		sfps_d = 1;
	}
	uint64_t old_increment = increment;
	increment = static_cast<uint64_t>(tfps_n) * sfps_d;
	decrement = static_cast<uint64_t>(tfps_d) * sfps_n;
	//We have to adjust balance by ratio of new and old incrments.
	balance = 1.0 * increment / old_increment * balance + 0.5;
}

uint64_t fpschanger::duration(uint64_t sduration)
{
	uint64_t count = 0;
	//FIXME: Do this faster.
	for(uint64_t i = 0; i < sduration; i++) {
		balance += increment;
		while(balance > 0) {
			count++;
			balance -= decrement;
		}
	}
	return count;
}
