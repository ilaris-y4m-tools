#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "fpschanger.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	uint64_t infr = 0;
	uint64_t outfr = 0;
	uint32_t ofps_n = 25;
	uint32_t ofps_d = 1;
	uint32_t fps_n = 0;
	uint32_t fps_d = 0;
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--fps=([1-9][0-9]*)", arg)) {
			try {
				fps_n = parse_value<unsigned>(r[1]);
				fps_d = 1;
			} catch(std::exception& e) {
				std::cerr << "changefps: Bad fps '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--fps=([1-9][0-9]*)/([1-9][0-9]*)", arg)) {
			try {
				fps_n = parse_value<unsigned>(r[1]);
				fps_d = parse_value<unsigned>(r[2]);
			} catch(std::exception& e) {
				std::cerr << "changefps: Bad fps '" << r[1] << "/" << r[2] << "'" << std::endl;
				return 1;
			}
		} else {
			std::cerr << "changefps: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		regex_results r;
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "420")
			framesize = 3 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "420p16")
			framesize = 6 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "422")
			framesize = 2 * strmh.width * strmh.height;
		else if(strmh.chroma == "422p16")
			framesize = 4 * strmh.width * strmh.height;
		else if(strmh.chroma == "444")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "444p16")
			framesize = 6 * strmh.width * strmh.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		if(strmh.fps_n && strmh.fps_d) {
			ofps_n = strmh.fps_n;
			ofps_d = strmh.fps_d;
		}
		if(fps_n && fps_d) {
			strmh.fps_n = fps_n;
			strmh.fps_d = fps_d;
		} else {
			fps_n = ofps_n;
			fps_d = ofps_d;
		}
		strmh.delete_extensions("VFR");
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		unsigned duration = 0;
		unsigned count = 0;
		fpschanger timer(fps_n, fps_d, ofps_n, ofps_d);
		std::string _framh, _framh2;
		std::vector<char> buffer;
		buffer.resize(framesize);
		struct yuv4mpeg_frame_header framh;

		//Frame loop.
next_input:
		if(!read_line2(in, _framh))
			return 0;	//End.
		framh = yuv4mpeg_frame_header(_framh);
		infr++;
		read_or_die(in, &buffer[0], buffer.size());
		duration = framh.duration;	//Default.
		count = timer.duration(duration);
		for(unsigned i = 0; i < count; i++) {
			outfr++;
			framh.duration = 1;
			std::string _framh2 = framh;
			write_or_die(out, _framh2);
			write_or_die(out, &buffer[0], framesize);
		}
		goto next_input;
	} catch(std::exception& e) {
		std::cerr << "changefps: Error: " << e.what() << std::endl;
		return 1;
	}
	std::cerr << "changefps: Read " << infr << " frames, wrote " << outfr << " frames." << std::endl;
	return 0;
}
