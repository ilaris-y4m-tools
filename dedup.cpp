#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	uint64_t infr = 0;
	uint64_t outfr = 0;
	unsigned maxelide = 11;
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--max=(0|[1-9][0-9]*)", arg)) {
			try {
				maxelide = parse_value<unsigned>(r[1]);
			} catch(std::exception& e) {
				std::cerr << "dedup: Bad max dedup '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else {
			std::cerr << "dedup: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "420")
			framesize = 3 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "420p16")
			framesize = 6 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "422")
			framesize = 2 * strmh.width * strmh.height;
		else if(strmh.chroma == "422p16")
			framesize = 4 * strmh.width * strmh.height;
		else if(strmh.chroma == "444")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "444p16")
			framesize = 6 * strmh.width * strmh.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		if(strmh.find_extension("VFR"))
			throw std::runtime_error("Can't dedup already VFR streams");
		strmh.extension.push_back("VFR");
		std::string _strmh = std::string(strmh);
		write_or_die(out, _strmh);

		unsigned duration = 0;
		bool first = true;
		std::string _framh, _framh2;
		std::vector<char> buffer;
		std::vector<char> buffer3;
		buffer.resize(framesize);
		buffer3.resize(framesize);
		//Write frames.
		while(true) {
			if(!read_line2(in, _framh))
				break;
			_framh2 = _framh;
			infr++;
			read_or_die(in, &buffer3[0], buffer3.size());
			if(duration == maxelide + 1)
				goto different;
			if(!first && memcmp(&buffer[0], &buffer3[0], framesize))
				goto different;
			if(first) {
				memcpy(&buffer[0], &buffer3[0], framesize);
				first = false;
			}
			duration++;
			continue;
different:
			outfr++;
			struct yuv4mpeg_frame_header framh(_framh);
			framh.duration = duration;
			std::string _framh2 = framh;
			write_or_die(out, _framh2);
			write_or_die(out, &buffer[0], framesize);
			memcpy(&buffer[0], &buffer3[0], framesize);
			duration = 1;
		}
		if(duration) {
			outfr++;
			struct yuv4mpeg_frame_header framh(_framh2);
			framh.duration = duration;
			_framh = framh;
			write_or_die(out, _framh);
			write_or_die(out, &buffer3[0], framesize);
		}
	} catch(std::exception& e) {
		std::cerr << "dedup: Error: " << e.what() << std::endl;
		return 1;
	}
	std::cerr << "Dedup: Read " << infr << " frames, wrote " << outfr << " frames." << std::endl;
	return 0;
}
