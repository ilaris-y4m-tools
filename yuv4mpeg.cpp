#include "yuv4mpeg.hpp"
#include <sstream>
#include <stdexcept>
#include <limits>
#include <fcntl.h>

namespace
{
	std::string extract_tok(const std::string& str, size_t& ptr)
	{
		size_t eptr = ptr;
		size_t strl = str.length();
		while(eptr < strl) {
			char ch = str[eptr];
			if(ch == '\t' || ch == '\v' || ch == '\r' || ch == ' ')
				break;
			eptr++;
		}
		std::string tmp = str;
		tmp = tmp.substr(ptr + 1, eptr - ptr - 1);
		ptr = eptr;
		return tmp;
	}

	template<typename T>
	T tonumber(const std::string& str)
	{
		T r = 0;
		size_t strl = str.length();
		if(str == "")
			throw std::runtime_error("Invalid number '" + str + "'");
		for(size_t i = 0; i < strl; i++) {
			if(str[i] < '0' || str[i] > '9')
				throw std::runtime_error("Invalid number '" + str + "'");
			if(r > std::numeric_limits<T>::max() / 10)
				throw std::runtime_error("Invalid number '" + str + "'");
			r *= 10;
			if(r + (str[i] - '0') < r)
				throw std::runtime_error("Invalid number '" + str + "'");
			r += (str[i] - '0');
		}
		return r;
	}

	void parse_ratio(const std::string& str, uint32_t& n, uint32_t& d)
	{
		std::string _str = str;
		size_t p = str.find_first_of(':');
		if(p > str.length())
			throw std::runtime_error("Invalid ratio '" + str + "'");
		n = tonumber<uint32_t>(_str.substr(0, p));
		d = tonumber<uint32_t>(_str.substr(p + 1));
	}

	std::string read_line(FILE* pipe)
	{
		std::ostringstream s;
		int ch;
		while(true) {
			ch = fgetc(pipe);
			if(ch == '\n' || ch < 0)
				return s.str();
			s << static_cast<char>(ch);
		}
	}
}

yuv4mpeg_stream_header::yuv4mpeg_stream_header()
{
	width = 0;
	height = 0;
	chroma = "420jpeg";
	interlace = '?';
	fps_n = 0;
	fps_d = 0;
	sar_n = 0;
	sar_d = 0;
}

yuv4mpeg_stream_header::yuv4mpeg_stream_header(const std::string& str)
{
	bool seen_width = false;
	bool seen_height = false;
	width = 0;
	height = 0;
	chroma = "420jpeg";
	interlace = '?';
	fps_n = 0;
	fps_d = 0;
	sar_n = 0;
	sar_d = 0;
	if(str.length() < 9 || str.substr(0, 9) != "YUV4MPEG2")
		throw std::runtime_error("Bad YUV4MPEG2 stream magic");
	size_t ptr = 9;
	size_t strl = str.length();
	std::string tok;
	while(ptr < strl) {
		char ch = str[ptr];
		switch(ch) {
		case '\t':
		case '\v':
		case '\r':
		case ' ':
			//Skip.
			ptr++;
			break;
		case 'A':
			tok = extract_tok(str, ptr);
			parse_ratio(tok, sar_n, sar_d);
			break;
		case 'C':
			chroma = extract_tok(str, ptr);
			break;
		case 'F':
			tok = extract_tok(str, ptr);
			parse_ratio(tok, fps_n, fps_d);
			break;
		case 'H':
			tok = extract_tok(str, ptr);
			height = tonumber<size_t>(tok);
			seen_height = true;
			break;
		case 'I':
			tok = extract_tok(str, ptr);
			if(tok.length() != 1)
				throw std::runtime_error("Invalid stream I-token");
			interlace = tok[0];
			break;
		case 'W':
			tok = extract_tok(str, ptr);
			width = tonumber<size_t>(tok);
			seen_width = true;
			break;
		case 'X':
			tok = extract_tok(str, ptr);
			if(tok == "")
				throw std::runtime_error("Empty extension token not allowed");
			extension.push_back(tok);
			break;
		default:
			throw std::runtime_error("Unknown token type '" + std::string(&ch, 1) + "'");
		}
	}
	if(!seen_width || !seen_height)
		throw std::runtime_error("W and H tokens are required");
}

yuv4mpeg_stream_header::yuv4mpeg_stream_header(FILE* pipe)
{
	*this = yuv4mpeg_stream_header(read_line(pipe));
}

yuv4mpeg_stream_header::operator std::string() const
{
	std::ostringstream h;
	h << "YUV4MPEG2 W" << width << " H" << height;
	if(chroma != "420jpeg")
		h << " C" << chroma;
	if(interlace != '?')
		h << " I" << interlace;
	if(fps_n || fps_d)
		h << " F" << fps_n << ":" << fps_d;
	if(sar_n || sar_d)
		h << " A" << sar_n << ":" << sar_d;
	for(auto i : extension)
		h << " X" << i;
	return h.str();
}

yuv4mpeg_frame_header::yuv4mpeg_frame_header()
{
	representation = 0;
	temporal = 0;
	chroma = 0;
	duration = 1;
}

yuv4mpeg_frame_header::yuv4mpeg_frame_header(const std::string& str)
{
	representation = 0;
	temporal = 0;
	chroma = 0;
	duration = 1;
	if(str.length() < 5 || str.substr(0, 5) != "FRAME")
		throw std::runtime_error("Bad YUV4MPEG2 frame magic");
	size_t ptr = 5;
	size_t strl = str.length();
	std::string tok;
	while(ptr < strl) {
		char ch = str[ptr];
		switch(ch) {
		case '\t':
		case '\v':
		case '\r':
		case ' ':
			//Skip.
			ptr++;
			break;
		case 'I':
			tok = extract_tok(str, ptr);
			if(tok.length() != 3)
				throw std::runtime_error("Invalid frame I-token");
			representation = tok[0];
			temporal = tok[1];
			chroma = tok[2];
			break;
		case 'X':
			tok = extract_tok(str, ptr);
			if(tok == "")
				throw std::runtime_error("Empty extension token not allowed");
			if(tok.length() > 9 && tok.substr(0, 9) == "duration=")
				duration = tonumber<uint32_t>(tok.substr(9));
			else
				extension.push_back(tok);
			break;
		default:
			throw std::runtime_error("Unknown token type '" + std::string(&ch, 1) + "'");
		}
	}
}

yuv4mpeg_frame_header::yuv4mpeg_frame_header(FILE* pipe)
{
	*this = yuv4mpeg_frame_header(read_line(pipe));
}

yuv4mpeg_frame_header::operator std::string() const
{
	std::ostringstream h;
	h << "FRAME";
	if(representation || temporal || chroma)
		h << " I" << representation << temporal << chroma;
	if(duration > 1)
		h << " Xduration=" << duration;
	for(auto i : extension)
		if(!regex_match("duration=.*", i))
			h << " X" << i;
	return h.str();
}

void mark_pipe_as_binary(FILE* pipe)
{
#if defined(_WIN32) || defined(_WIN64)
	setmode(fileno(pipe), O_BINARY);
#endif
}

void read_or_die(FILE* pipe, void* data, size_t datasize)
{
	char* _data = reinterpret_cast<char*>(data);
	while(datasize > 0) {
		size_t r;
		size_t s = (datasize > 4096) ? 4096 : datasize;
		r = fread(_data, 1, s, pipe);
		if(r < s)
			throw std::runtime_error("Error reading from file");
		datasize -= r;
		_data += r;
	}
}

bool read_or_die2(FILE* pipe, void* data, size_t datasize)
{
	bool first = true;
	char* _data = reinterpret_cast<char*>(data);
	while(datasize > 0) {
		size_t r;
		size_t s = (datasize > 4096) ? 4096 : datasize;
		r = fread(_data, 1, s, pipe);
		if(r == 0 && first)
			return false;
		if(r < s)
			throw std::runtime_error("Error reading from file");
		datasize -= r;
		_data += r;
		first = false;
	}
	return true;
}

void write_or_die(FILE* pipe, const void* data, size_t datasize)
{
	const char* _data = reinterpret_cast<const char*>(data);
	while(datasize > 0) {
		size_t r;
		size_t s = (datasize > 4096) ? 4096 : datasize;
		r = fwrite(_data, 1, s, pipe);
		if(r < s)
			throw std::runtime_error("Error writing to file");
		datasize -= r;
		_data += r;
	}
}

void write_or_die(FILE* pipe, const std::string& data)
{
	std::vector<char> v;
	v.resize(data.size() + 1);
	std::copy(data.begin(), data.end(), v.begin());
	v[v.size() - 1] = '\n';
	write_or_die(pipe, &v[0], v.size());
}

void get_512_scaling(size_t w, size_t h, uint32_t sar_n, uint32_t sar_d, size_t& w512, size_t& h512)
{
	if(!sar_n || !sar_d) {
		w512 = w;
		h512 = h;
		return;
	}
	if(sar_d < sar_n) {
		//w / h < sar_n / sar_d, widen the screen.
		w512 = static_cast<uint64_t>(w) * sar_n / sar_d;
		h512 = h;
		if(w512 & 1)
			w512++;
	} else if(sar_d > sar_n) {
		//w / h > sar_n / sar_d, tallen the screen.
		w512 = w;
		h512 = static_cast<uint64_t>(h) * sar_d / sar_n;
		if(h512 & 1)
			h512++;
	} else {
		w512 = w;
		h512 = h;
	}
}

bool read_line2(FILE* pipe, std::string& line)
{
	std::ostringstream s;
	int ch;
	bool first = true;
	while(true) {
		ch = fgetc(pipe);
		if(ch == '\n' || ch < 0)  {
			line = s.str();
			return (ch >= 0 || !first);
		}
		s << static_cast<char>(ch);
		first = false;
	}
}

regex_results yuv4mpeg_find_extension(const std::vector<std::string>& exts, const std::string& regexp)
{
	for(auto i : exts) {
		regex_results r;
		if(r = regex(regexp, i))
			return r;
	}
	return regex_results();
}

void yuv4mpeg_delete_extensions(std::vector<std::string>& exts, const std::string& regexp)
{
	bool redo = false;
	do {
		redo = false;
		for(auto i = exts.begin(); i != exts.end(); i++)
			if(regex_match(regexp, *i)) {
				exts.erase(i);
				redo = true;
				break;
			}
	} while(redo);
}
