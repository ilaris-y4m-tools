#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	uint64_t last_interval = 0;
	unsigned interval = 5;
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--interval=([1-9][0-9]*)", arg)) {
			try {
				interval = parse_value<unsigned>(r[1]);
			} catch(std::exception& e) {
				std::cerr << "timeshow: Bad interval '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else {
			std::cerr << "timeshow: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	mark_pipe_as_binary(in);
	
	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "420")
			framesize = 3 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "420p16")
			framesize = 6 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "422")
			framesize = 2 * strmh.width * strmh.height;
		else if(strmh.chroma == "422p16")
			framesize = 4 * strmh.width * strmh.height;
		else if(strmh.chroma == "444")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "444p16")
			framesize = 6 * strmh.width * strmh.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		bool vfr = strmh.find_extension("VFR");

		uint64_t duration = 0;
		std::string _framh, _framh2;
		std::vector<char> buffer;
		buffer.resize(framesize);
		double framerate = (strmh.fps_n && strmh.fps_d) ? (1.0 * strmh.fps_n / strmh.fps_d) : 25;
		//Loop frames.
		while(true) {
			if(!read_line2(in, _framh))
				break;
			read_or_die(in, &buffer[0], buffer.size());
			struct yuv4mpeg_frame_header framh(_framh);
			duration += (vfr ? framh.duration : 1);
			uint64_t seconds = duration / framerate;
			uint64_t cur_interval = seconds / interval;
			if(cur_interval > last_interval) {
				last_interval = cur_interval;
				std::cerr << "timeshow: " << seconds << "s done.\e[K" << std::endl;
			}
		}
	} catch(std::exception& e) {
		std::cerr << "timeshow: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
