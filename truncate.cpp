#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "marker.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	timemarker start, end;
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--end=(.*)", arg)) {
			try {
				end = timemarker(r[1]);
			} catch(std::exception& e) {
				std::cerr << "truncate: Bad end '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--start=(.*)", arg)) {
			try {
				start = timemarker(r[1]);
			} catch(std::exception& e) {
				std::cerr << "truncate: Bad start '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else {
			std::cerr << "truncate: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		regex_results r;
		double fps = 25;
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "420")
			framesize = 3 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "420p16")
			framesize = 6 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "422")
			framesize = 2 * strmh.width * strmh.height;
		else if(strmh.chroma == "422p16")
			framesize = 4 * strmh.width * strmh.height;
		else if(strmh.chroma == "444")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "444p16")
			framesize = 6 * strmh.width * strmh.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		if(strmh.fps_n && strmh.fps_d) {
			fps = 1.0 * strmh.fps_n / strmh.fps_d;
		}
		uint64_t startframe = start.get_frame(fps, 0);
		uint64_t endframe = end.get_frame(fps, 0xFFFFFFFFFFFFFFFFULL);
		if(startframe > endframe)
			throw std::runtime_error("Startpoint is after endpoint");
		write_or_die(out, static_cast<std::string>(strmh));

		uint64_t curframe = 0;
		std::string _framh, _framh2;
		std::vector<char> buffer;
		buffer.resize(framesize);
		struct yuv4mpeg_frame_header framh;

		//Frame loop.
		while(true) {
			unsigned duration = 1;
			if(!read_line2(in, _framh))
				return 0;	//End.
			framh = yuv4mpeg_frame_header(_framh);
			read_or_die(in, &buffer[0], buffer.size());
			duration = framh.duration;
			uint64_t nextframe = curframe + duration;
			if(curframe > endframe)
				break;
			if(nextframe < startframe) {
				curframe = nextframe;
				continue;
			}
			bool stradle_start = (curframe < startframe);
			bool stradle_end = (nextframe > endframe);
			if(stradle_start && stradle_end)
				duration = endframe - startframe;
			else if(stradle_start)
				duration = nextframe - startframe;
			else if(stradle_end)
				duration = endframe - curframe;
			framh.duration = duration;
			write_or_die(out, static_cast<std::string>(framh));
			write_or_die(out, &buffer[0], buffer.size());
			curframe = nextframe;
		}
	} catch(std::exception& e) {
		std::cerr << "truncate: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
