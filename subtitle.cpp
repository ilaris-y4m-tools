#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "marker.hpp"
#include "rendertext.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	subtitle_render_context rctx;
	std::vector<pre_subtitle*> presubs;
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		try {
			if(!rctx.argument(arg, presubs)) {
				std::cerr << "subtitle: Unrecognized option '" << arg << "'" << std::endl;
				return 1;
			}
		} catch(std::exception& e) {
			std::cerr << "subtitle: Error in option '" << arg << "': " << e.what() << std::endl;
			return 1;
		}
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "420")
			framesize = 3 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "420p16")
			framesize = 6 * strmh.width * strmh.height / 2;
		else if(strmh.chroma == "422")
			framesize = 2 * strmh.width * strmh.height;
		else if(strmh.chroma == "422p16")
			framesize = 4 * strmh.width * strmh.height;
		else if(strmh.chroma == "444")
			framesize = 3 * strmh.width * strmh.height;
		else if(strmh.chroma == "444p16")
			framesize = 6 * strmh.width * strmh.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		std::vector<subtitle*> subs = subtitle::from_presub(presubs, strmh);
		write_or_die(out, static_cast<std::string>(strmh));

		uint64_t curframe = 0;
		std::string _framh, _framh2;
		std::vector<char> buffer;
		buffer.resize(framesize);
		struct yuv4mpeg_frame_header framh;

		//Frame loop.
		while(true) {
			regex_results r;
			unsigned duration = 1;
			if(!read_line2(in, _framh))
				return 0;	//End.
			framh = yuv4mpeg_frame_header(_framh);
			read_or_die(in, &buffer[0], buffer.size());
			duration = framh.duration;
			for(auto i : subs)
				i->stamp(reinterpret_cast<uint8_t*>(&buffer[0]), strmh.width, strmh.height, curframe);
			write_or_die(out, static_cast<std::string>(framh));
			write_or_die(out, &buffer[0], buffer.size());
			curframe += duration;
		}
	} catch(std::exception& e) {
		std::cerr << "subtitle: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
