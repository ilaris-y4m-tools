#ifndef _yuv4mpeg_hpp_included_
#define _yuv4mpeg_hpp_included_

#include <cstdint>
#include <string>
#include <vector>
#include "parseval.hpp"

regex_results yuv4mpeg_find_extension(const std::vector<std::string>& exts, const std::string& regex);
void yuv4mpeg_delete_extensions(std::vector<std::string>& exts, const std::string& regex);

struct yuv4mpeg_stream_header
{
	yuv4mpeg_stream_header();
	yuv4mpeg_stream_header(const std::string& str);
	yuv4mpeg_stream_header(FILE* pipe);
	operator std::string() const;
	regex_results find_extension(const std::string& regex) const
	{
		return yuv4mpeg_find_extension(extension, regex);
	}
	void delete_extensions(const std::string& regex) { yuv4mpeg_delete_extensions(extension, regex); }
	enum interlace_v
	{
		INT_UNKNOWN = '?',
		INT_PROGRESSIVE = 'p',
		INT_TOP_FIRST = 't',
		INT_BOTTOM_FIRST = 'b',
		INT_MIXED = 'm'
	};
	size_t width;
	size_t height;
	std::string chroma;
	char interlace;
	uint32_t fps_n;
	uint32_t fps_d;
	uint32_t sar_n;
	uint32_t sar_d;
	std::vector<std::string> extension;
};

struct yuv4mpeg_frame_header
{
	yuv4mpeg_frame_header();
	yuv4mpeg_frame_header(const std::string& str);
	yuv4mpeg_frame_header(FILE* pipe);
	operator std::string() const;
	regex_results find_extension(const std::string& regex) const
	{
		return yuv4mpeg_find_extension(extension, regex);
	}
	void delete_extensions(const std::string& regex) { yuv4mpeg_delete_extensions(extension, regex); }
	enum representation_v
	{
		REP_TOP_FIRST = 't',
		REP_TOP_FIRST_REPEAT = 'T',
		REP_BOTTOM_FIRST = 'b',
		REP_BOTTOM_FIRST_REPEAT = 'B',
		REP_PROGRESSIVE = '1',
		REP_PROGRESSIVE_DBL = '2',
		REP_PROGRESSIVE_TRIPL = '3'
	};
	enum temporal_v
	{
		TMP_PROGRESSIVE = 'p',
		TMP_INTERLACED = 'i'
	};
	enum chroma_v
	{
		CHR_PROGRESIVE = 'p',
		CHR_INTERLACED = 'i',
		CHR_UNKNOWN = '?'
	};
	char representation;
	char temporal;
	char chroma;
	uint32_t duration;
	std::vector<std::string> extension;
};

void mark_pipe_as_binary(FILE* pipe);
void read_or_die(FILE* pipe, void* data, size_t datasize);
bool read_or_die2(FILE* pipe, void* data, size_t datasize);
bool read_line2(FILE* pipe, std::string& str);
void write_or_die(FILE* pipe, const void* data, size_t datasize);
void write_or_die(FILE* pipe, const std::string& data);
void get_512_scaling(size_t w, size_t h, uint32_t sar_n, uint32_t sar_d, size_t& w512, size_t& h512);

#endif
