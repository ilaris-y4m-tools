#ifndef _fpschanger_hpp_included_
#define _fpschanger_hpp_included_

#include <cstdint>

class fpschanger
{
public:
	fpschanger(uint32_t tfps_n, uint32_t tfps_d, uint32_t sfps_n, uint32_t sfps_d);
	void change_source_rate(uint32_t sfps_n, uint32_t sfps_d);
	uint64_t duration(uint64_t sduration);
private:
	uint64_t increment;
	uint64_t decrement;
	int64_t balance;
	uint32_t tfps_n;
	uint32_t tfps_d;
};

#endif
