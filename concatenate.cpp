#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "fpschanger.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	std::vector<std::string> files;
	std::vector<FILE*> in;
	bool stdin_used = false;
	unsigned headernum = 0;
	size_t framesize = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--primary-header=(0|[1-9][0-9]*)", arg)) {
			try {
				headernum = parse_value<unsigned>(r[1]);
			} catch(std::exception& e) {
				std::cerr << "concatenate: Bad end '" << r[1] << "'" << std::endl;
				return 1;
			}
		} else if(r = regex("--.*", arg)) {
			std::cerr << "concatenate: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		} else {
			files.push_back(arg);
		}
	}

	if(headernum >= files.size()) {
		std::cerr << "concatenate: Invalid primary header number" << std::endl;
		return 1;
	}

	//Open files.
	FILE* out = stdout;
	mark_pipe_as_binary(out);
	for(auto i : files) {
		if(i == "-") {
			if(stdin_used) {
				std::cerr << "concatenate: Stdin can only be used once" << std::endl;
				return 1;
			}
			stdin_used = true;
			in.push_back(stdin);
			mark_pipe_as_binary(stdin);
		} else {
			FILE* _in = fopen(i.c_str(), "rb");
			if(!_in) {
				std::cerr << "concatenate: Can't open '" << i << "'" << std::endl;
				return 1;
			}
			in.push_back(_in);
		}
	}
	
	//Fixup header.
	try {
		regex_results r;
		//Read all stream headers.
		std::vector<yuv4mpeg_stream_header> strmhs;
		yuv4mpeg_stream_header phdr;
		for(auto i : in) {
			struct yuv4mpeg_stream_header strmh(i);
			strmhs.push_back(strmh);
		}
		phdr = strmhs[headernum];

		//Check that all headers are compatible.
		for(auto i : strmhs) {
			if(i.width != phdr.width || i.height != phdr.height)
				throw std::runtime_error("Streams must all have the same size");
			if(i.chroma != phdr.chroma)
				throw std::runtime_error("Streams must all have the same chroma");
			if(i.interlace != phdr.interlace)
				throw std::runtime_error("Streams must all have the same interlace");
		}

		//Get frame size.
		if(phdr.chroma == "rgb")
			framesize = 3 * phdr.width * phdr.height;
		else if(phdr.chroma == "420")
			framesize = 3 * phdr.width * phdr.height / 2;
		else if(phdr.chroma == "420p16")
			framesize = 6 * phdr.width * phdr.height / 2;
		else if(phdr.chroma == "422")
			framesize = 2 * phdr.width * phdr.height;
		else if(phdr.chroma == "422p16")
			framesize = 4 * phdr.width * phdr.height;
		else if(phdr.chroma == "444")
			framesize = 3 * phdr.width * phdr.height;
		else if(phdr.chroma == "444p16")
			framesize = 6 * phdr.width * phdr.height;
		else
			throw std::runtime_error("Unsupported input chroma type '" + phdr.chroma + "'");

		write_or_die(out, static_cast<std::string>(phdr));

		std::string _framh;
		std::vector<char> buffer;
		buffer.resize(framesize);

		fpschanger timer(phdr.fps_n, phdr.fps_d, phdr.fps_n, phdr.fps_d);
		bool tvfr = phdr.find_extension("VFR");

		//Frame loop.
		size_t index = 0;
		for(auto i : in) {
			timer.change_source_rate(strmhs[index].fps_n, strmhs[index].fps_d);
			bool svfr = strmhs[index].find_extension("VFR");
			while(true) {
				uint64_t duration = 1;
				if(!read_line2(i, _framh))
					break;		//Next!
				read_or_die(i, &buffer[0], buffer.size());
				struct yuv4mpeg_frame_header framh(_framh);
				if(svfr)
					duration = framh.duration;
				duration = timer.duration(duration);
				if(tvfr) {
					framh.duration = duration;
					write_or_die(out, framh);
					write_or_die(out, &buffer[0], buffer.size());
				} else {
					framh.duration = 1;
					for(uint64_t i = 0; i < duration; i++) {
						write_or_die(out, framh);
						write_or_die(out, &buffer[0], buffer.size());
					}
				}
			}
			index++;
		}
	} catch(std::exception& e) {
		std::cerr << "concatenate: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
