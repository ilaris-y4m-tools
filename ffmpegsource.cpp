extern "C" {
#ifndef UINT64_C
#define UINT64_C(val) val##ULL
#endif
#include <libavformat/avformat.h>
#include <libavutil/mem.h>
#include <libavutil/pixdesc.h>
#include <libavutil/dict.h>
#include <libswscale/swscale.h>
}
#include "yuv4mpeg.hpp"
#include <iostream>
#include <stdexcept>

struct pts_info
{
	bool valid;
	uint64_t pts;
};

bool read_frame_data(AVFormatContext* lavf, AVCodecContext* codec, AVFrame* frame, int vstream, pts_info& pts,
	bool indeterminate = false);

bool read_frame_data(AVFormatContext* lavf, AVCodecContext* codec, AVFrame* frame, int vstream, pts_info& pts,
	bool indeterminate)
{
	int r;
	int done = 0;
	AVPacket* pkt = new AVPacket;
	av_init_packet(pkt);
	bool first = true;
again:
	r = av_read_frame(lavf, pkt);
	if(first && r < 0 && indeterminate)
		return false;
	first = false;
	if(pkt->stream_index == vstream) {
		if(r < 0) pkt->size = 0;	//Don't crash.
		codec->reordered_opaque = pkt->pts;
		if(avcodec_decode_video2(codec, frame, &done, pkt) < 0) {
			av_free_packet(pkt);
			delete pkt;
			throw std::runtime_error("Failed to decode frame from video");
		}
	} else {
		if(r >= 0) {
			//Not wanted.
			av_free_packet(pkt);
			av_init_packet(pkt);
		}
	}
	if(!done) {
		if(r >= 0)
			goto again;
		else {
			av_free_packet(pkt);
			delete pkt;
			throw std::runtime_error("Failed to decode packet from video");
		}
	}
	if(pts.valid) {
		pts.pts = 0;
		if(codec->has_b_frames && frame->reordered_opaque != AV_NOPTS_VALUE)
			pts.pts = frame->reordered_opaque;
		else if(pkt->dts != AV_NOPTS_VALUE)
			pts.pts = pkt->dts;
		else
			pts.valid = false;
	}
	av_free_packet(pkt);
	delete pkt;
	return true;
}

void copy_frame(char* out, AVFrame* frame, size_t width, size_t height, AVPixelFormat csp)
{
	if(csp == AV_PIX_FMT_RGB24) {
		uint8_t* rfptr = frame->data[0];
		for(size_t i = 0; i < height; i++) {
			memcpy(out + 3 * width * i, rfptr, 3 * width);
			rfptr += frame->linesize[0];
		}
	} else {
		static SwsContext* ctx1 = NULL;
		ctx1 = sws_getCachedContext(ctx1, width, height, csp, width, height, AV_PIX_FMT_RGB24,
			SWS_GAUSS, NULL, NULL, NULL);
		uint8_t* out_ptr[1];
		int out_stride[1];
		out_stride[0] = 3 * width;
		out_ptr[0] = reinterpret_cast<uint8_t*>(out);
		sws_scale(ctx1, frame->data, frame->linesize, 0, height, out_ptr, out_stride);
	}
}

namespace
{
#if LIBAVCODEC_VERSION_INT >= AV_VERSION_INT(55, 28, 1)
	AVFrame* (*f_alloc)() = av_frame_alloc;
	void (*f_prepare)(AVFrame*) = av_frame_unref;
	void (*f_free)(AVFrame**) = av_frame_free;
#else
	AVFrame* (*f_alloc)() = avcodec_alloc_frame;
	void (*f_prepare)(AVFrame*) = avcodec_get_frame_defaults;
#if LIBAVCODEC_VERSION_INT >= AV_VERSION_INT(54, 28, 0)
	void (*f_free)(AVFrame**) = avcodec_free_frame;
#else
	void (*f_free)(AVFrame**) = av_freep;
#endif
#endif
}

int main(int argc, char** argv)
{
	bool vspec = false;
	std::string videofile;
	bool fspec = false;
	std::string iformat;
	bool nosync = false;
	bool ignorelength = false;
	uint32_t sar_n = 0;
	uint32_t sar_d = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--format=(.*)", arg)) {
			iformat = r[1];
			fspec = true;
		} else if(r = regex("--sar=([1-9][0-9]*)/([1-9][0-9]*)", arg)) {
			sar_n = parse_value<uint32_t>(r[1]);
			sar_d = parse_value<uint32_t>(r[2]);
		} else if(r = regex("--no-sync", arg)) {
			nosync = true;
		} else if(r = regex("--ignore-length", arg)) {
			ignorelength = true;
		} else if(r = regex("--.*", arg)) {
			std::cerr << "ffmpegsource: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		} else {
			if(vspec) {
				std::cerr << "ffmpegsource: Only one video can be specified" << std::endl;
				return 1;
			}
			vspec = true;
			videofile = arg;
		}
	}
	if(!vspec) {
		std::cerr << "ffmpegsource: Video to load required" << std::endl;
		return 1;
	} else if(videofile == "-")
		videofile = "pipe:";

	FILE* out = stdout;
	mark_pipe_as_binary(out);

	try {
		//Some basic init.
		av_register_all();
		AVFrame* frame = f_alloc();
		if(!frame)
			throw std::bad_alloc();

		//Find the input format.
		AVInputFormat* format = NULL;
		if(fspec) {
			format = av_find_input_format(iformat.c_str());
			if(!format)
				throw std::runtime_error("Trying to force invalid file format");
		}

		//Open the video.
		AVFormatContext* lavf = NULL;
		if(avformat_open_input(&lavf, videofile.c_str(), format, NULL))
			throw std::runtime_error("Can't open video file");

		//Find the video strema.
		if(avformat_find_stream_info(lavf, NULL) < 0)
			throw std::runtime_error("Can't get information about streams in video");
		unsigned vstream = lavf->nb_streams;
		for(unsigned i = 0; i < lavf->nb_streams; i++)
			if(lavf->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
				vstream = i;
				break;
			}
		if(vstream == lavf->nb_streams)
			throw std::runtime_error("Can't find valid video stream in video");
		auto sinfo = lavf->streams[vstream];
		AVCodecContext* codec = sinfo->codec;
		uint32_t fps_n = sinfo->time_base.den ? sinfo->time_base.den : sinfo->avg_frame_rate.num;
		uint32_t fps_d = sinfo->time_base.num ? sinfo->time_base.num : sinfo->avg_frame_rate.den;
		if(avcodec_open2(codec, avcodec_find_decoder(codec->codec_id), NULL))
			throw std::runtime_error("Can't find decoder for the video stream");

		//Decode the first frame.
		pts_info pts;
		pts.valid = !nosync;
		pts.pts = 0;
		f_prepare(frame);
		read_frame_data(lavf, codec, frame, vstream, pts);
		uint64_t frames = sinfo->nb_frames;

		//Emit the yuv4mpeg stream header.
		yuv4mpeg_stream_header strmh;
		strmh.width = codec->width;
		strmh.height = codec->height;
		strmh.chroma = "rgb";
		strmh.interlace = (frame->interlaced_frame ? (frame->top_field_first ? 't' : 'b') : 'p');
		strmh.fps_n = fps_n;
		strmh.fps_d = fps_d;
		strmh.sar_n = sar_n;
		strmh.sar_d = sar_d;
		if(pts.valid)
			strmh.extension.push_back("VFR");
		write_or_die(out, static_cast<std::string>(strmh));
		std::vector<char> framedata;
		framedata.resize(3 * strmh.width * strmh.height);

		//Emit frame.
		if(pts.valid && pts.pts > 0) {
			yuv4mpeg_frame_header fh;
			fh.duration = pts.pts;
			write_or_die(out, static_cast<std::string>(fh));
			write_or_die(out, &framedata[0], framedata.size());
		}
		copy_frame(&framedata[0], frame, strmh.width, strmh.height, codec->pix_fmt);
		for(uint64_t i = 1; ignorelength || i < frames; i++) {
			uint64_t old_pts = pts.pts;
			bool x = read_frame_data(lavf, codec, frame, vstream, pts, ignorelength);
			if(!x)
				break;
			uint64_t duration = 1;
			if(pts.valid)
				duration = pts.pts - old_pts;
			yuv4mpeg_frame_header fh;
			fh.duration = duration;
			write_or_die(out, static_cast<std::string>(fh));
			write_or_die(out, &framedata[0], framedata.size());
			copy_frame(&framedata[0], frame, strmh.width, strmh.height, codec->pix_fmt);
		}
		//For the last frame, no duration is available, assume 1.
		{
			yuv4mpeg_frame_header fh;
			write_or_die(out, static_cast<std::string>(fh));
			write_or_die(out, &framedata[0], framedata.size());
		}

		//Clean up.
		avcodec_close(codec);
		avformat_close_input(&lavf);
		f_free(&frame);
	} catch(std::exception& e) {
		std::cerr << "ffmpegsource: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
