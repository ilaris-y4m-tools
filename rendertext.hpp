#ifndef _rendertext_hpp_included_
#define _rendertext_hpp_included_

#include <map>
#include "png.hpp"
#include "yuv4mpeg.hpp"
#include "marker.hpp"

struct text_render_parameters
{
	text_render_parameters();
	std::string fontfile;
	unsigned fontsize;
	signed text_alignment;
	unsigned text_spacing;
	long fontface;
	uint32_t fg_color;
	uint32_t halo_color;
	uint32_t bg_color;
	signed halo_thickness;
	bool argument(const std::string& arg);
	parsed_png& render(const std::string& str);
	std::pair<size_t, size_t> render_size(const std::string& str);
};

struct pre_subtitle
{
	pre_subtitle();
	~pre_subtitle();
	parsed_png* image;
	ssize_t xpos;
	ssize_t ypos;
	bool xabsolute;
	bool yabsolute;
	timemarker start;
	timemarker duration;
};

struct subtitle
{
	subtitle(const pre_subtitle& presub, const yuv4mpeg_stream_header& strmh);
	~subtitle();
	uint8_t* data;
	uint32_t* alpha;
	size_t width;
	size_t height;
	size_t planesep;
	unsigned planes;
	bool bits16;
	ssize_t x;
	ssize_t y;
	uint64_t frame;
	uint64_t duration;
	static std::vector<subtitle*> from_presub(const std::vector<pre_subtitle*>& presubs,
		const yuv4mpeg_stream_header& strmh);
	void stamp(uint8_t* data, size_t iwidth, size_t iheight, uint64_t frameno) const;
};

struct subtitle_render_context
{
	subtitle_render_context();
	text_render_parameters tparams;
	ssize_t xpos;
	ssize_t ypos;
	bool xabsolute;
	bool yabsolute;
	timemarker duration;
	bool argument(const std::string& arg, pre_subtitle*& presub);
	bool argument(const std::string& arg, std::vector<pre_subtitle*>& presubs)
	{
		pre_subtitle* presub = NULL;
		bool ret = argument(arg, presub);
		if(presub)
			presubs.push_back(presub);
		return ret;
	}
};

#endif
