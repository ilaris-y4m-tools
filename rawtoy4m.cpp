#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include <iostream>

int main(int argc, char** argv)
{
	const char* interlace_types = "bpt";
	size_t width = 0, height = 0;
	uint32_t fps_n = 0, fps_d = 0;
	uint32_t sar_n = 0, sar_d = 0;
	int interlace = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		regex_results r;
		if(r = regex("--resolution=([1-9][0-9]*)x([1-9][0-9]*)", arg)) {
			width = parse_value<uint32_t>(r[1]);
			height = parse_value<uint32_t>(r[2]);
		} else if(r = regex("--fps=([1-9][0-9]*)/([1-9][0-9]*)", arg)) {
			fps_n = parse_value<uint32_t>(r[1]);
			fps_d = parse_value<uint32_t>(r[2]);
		} else if(r = regex("--fps=([1-9][0-9]*)", arg)) {
			fps_n = parse_value<uint32_t>(r[1]);
			fps_d = 1;
		} else if(r = regex("--sar=([1-9][0-9]*)/([1-9][0-9]*)", arg)) {
			sar_n = parse_value<uint32_t>(r[1]);
			sar_d = parse_value<uint32_t>(r[2]);
		} else if(r = regex("--progressive", arg)) {
			interlace = 0;
		} else if(r = regex("--interlace=top", arg)) {
			interlace = 1;
		} else if(r = regex("--interlace=bottom", arg)) {
			interlace = -1;
		} else {
			std::cerr << "rawtoy4m: Unrecognized option '" << arg << "'" << std::endl;
			return 1;
		}
	}
	if(!width || !height) {
		std::cerr << "rawtoy4m: --resolution=<w>x<h> required" << std::endl;
		return 1;
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);

	//Write header.
	struct yuv4mpeg_stream_header strmh;
	strmh.width = width;
	strmh.height = height;
	strmh.chroma = "rgb";
	strmh.interlace = interlace_types[interlace + 1];
	strmh.fps_n = fps_n;
	strmh.fps_d = fps_d;
	strmh.sar_n = sar_n;
	strmh.sar_d = sar_d;
	std::string _strmh = std::string(strmh);
	write_or_die(out, _strmh);

	std::vector<char> buffer;
	buffer.resize(3 * width * height);
	//Write frames.
	while(true) {
		if(!read_or_die2(in, &buffer[0], buffer.size()))
			break;
		struct yuv4mpeg_frame_header framh;
		std::string _framh = std::string(framh);
		write_or_die(out, _framh);
		write_or_die(out, &buffer[0], buffer.size());
	}
	return 0;
}
