#include "yuv4mpeg.hpp"
#include "parseval.hpp"
#include "marker.hpp"
#include "rendertext.hpp"
#include <iostream>

struct crop_params
{
	size_t x, y, w, h;
};

template<unsigned s, bool xdiv, bool ydiv>
void do_crop_g(char* out, char* content, size_t fsize, size_t height, crop_params p)
{
	size_t off = fsize / height * (p.y >> (ydiv ? 1 : 0)) + s * (p.x >> (xdiv ? 1 : 0));
	size_t len = s * (p.w >> (xdiv ? 1 : 0));
	size_t stride = fsize / height;
	for(size_t i = 0; i < (p.h >> (ydiv ? 1 : 0)); i++)
		memcpy(out + len * i, content + off + stride * i, len); 
}

void do_crop(char* out, char* content, size_t fsize, size_t height, unsigned mode, crop_params p)
{
	if(mode == 0) {
		//Mode 0: RGB
		do_crop_g<3, false, false>(out, content, fsize, height, p);
	} else if(mode == 1) {
		//Mode 1: YUV420 8-bit.
		size_t psize = fsize * 2 / 3;
		size_t psize2 = p.w * p.h;
		do_crop_g<1, false, false>(out, content, psize, height, p);
		do_crop_g<1, true, true>(out + psize2, content + psize, psize >> 2, height >> 1, p);
		do_crop_g<1, true, true>(out + psize2 + (psize2 >> 2), content + psize + (psize >> 2), psize >> 2,
			height >> 1, p);
	} else if(mode == 2) {
		//Mode 2: YUV420 16-bit.
		size_t psize = fsize * 2 / 3;
		size_t psize2 = p.w * p.h * 2;
		do_crop_g<2, false, false>(out, content, psize, height, p);
		do_crop_g<2, true, true>(out + psize2, content + psize, psize >> 2, height >> 1, p);
		do_crop_g<2, true, true>(out + psize2 + (psize2 >> 2), content + psize + (psize >> 2), psize >> 2,
			height >> 1, p);
	} else if(mode == 3) {
		//Mode 3: YUV422 8-bit.
		size_t psize = fsize / 2;
		size_t psize2 = p.w * p.h;
		do_crop_g<1, false, false>(out, content, psize, height, p);
		do_crop_g<1, false, true>(out + psize2, content + psize, psize >> 1, height >> 1, p);
		do_crop_g<1, false, true>(out + psize2 + (psize2 >> 1), content + psize + (psize >> 1), psize >> 1,
			height >> 1, p);
	} else if(mode == 4) {
		//Mode 4: YUV422 16-bit.
		size_t psize = fsize / 2;
		size_t psize2 = p.w * p.h * 2;
		do_crop_g<2, false, false>(out, content, psize, height, p);
		do_crop_g<2, false, true>(out + psize2, content + psize, psize >> 1, height >> 1, p);
		do_crop_g<2, false, true>(out + psize2 + (psize2 >> 1), content + psize + (psize >> 1), psize >> 1,
			height >> 1, p);
	} else if(mode == 5) {
		//Mode 5: YUV444 8-bit.
		size_t psize = fsize / 3;
		size_t psize2 = p.w * p.h;
		do_crop_g<1, false, false>(out, content, psize, height, p);
		do_crop_g<1, false, false>(out + psize2, content + psize, psize, height, p);
		do_crop_g<1, false, false>(out + psize2 + psize2, content + psize + psize, psize, height, p);
	} else if(mode == 6) {
		//Mode 6: YUV444 16-bit.
		size_t psize = fsize / 3;
		size_t psize2 = p.w * p.h * 2;
		do_crop_g<2, false, false>(out, content, psize, height, p);
		do_crop_g<2, false, false>(out + psize2, content + psize, psize, height, p);
		do_crop_g<2, false, false>(out + psize2 + psize2, content + psize + psize, psize, height, p);
	}
}

int main(int argc, char** argv)
{
	size_t xmod = 1;
	size_t ymod = 1;
	crop_params cp;
	cp.x = cp.y = cp.w = cp.h = 0;
	size_t framesize = 0;
	size_t framesize2 = 0;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		try {
			regex_results r;
			if(r = regex("--.*", arg)) {
				std::cerr << "crop: Unrecognized option '" << arg << "'" << std::endl;
				return 1;
			} else {
				if(r = regex("([0-9]+),([0-9]+)-([0-9]+),([0-9]+)", arg)) {
					cp.x = parse_value<size_t>(r[1]);
					cp.y = parse_value<size_t>(r[2]);
					cp.w = parse_value<size_t>(r[3]) - cp.x;
					cp.h = parse_value<size_t>(r[4]) - cp.y;
				} else if(r = regex("([0-9]+),([0-9]+),([0-9]+),([0-9]+)", arg)) {
					cp.x = parse_value<size_t>(r[1]);
					cp.y = parse_value<size_t>(r[2]);
					cp.w = parse_value<size_t>(r[3]);
					cp.h = parse_value<size_t>(r[4]);
				} else {
					std::cerr << "crop: Unrecognized option '" << arg << "'" << std::endl;
					return 1;
				}
			}
		} catch(std::exception& e) {
			std::cerr << "crop: Error in option '" << arg << "': " << e.what() << std::endl;
			return 1;
		}
	}

	if(!cp.w || !cp.h) {
		std::cerr << "crop: 0x0 output not allowed" << std::endl;
		return 1;
	}

	//Open files.
	FILE* in = stdin;
	FILE* out = stdout;
	mark_pipe_as_binary(in);
	mark_pipe_as_binary(out);
	
	//Fixup header.
	try {
		unsigned mode;
		struct yuv4mpeg_stream_header strmh(in);
		if(strmh.chroma == "rgb") {
			framesize = 3 * strmh.width * strmh.height;
			framesize2 = 3 * cp.w * cp.h;
			mode = 0;
		} else if(strmh.chroma == "420") {
			framesize = 3 * strmh.width * strmh.height / 2;
			framesize2 = 3 * cp.w * cp.h / 2;
			xmod = ymod = 2;
			mode = 1;
		} else if(strmh.chroma == "420p16") {
			framesize = 6 * strmh.width * strmh.height / 2;
			framesize2 = 6 * cp.w * cp.h / 2;
			xmod = ymod = 2;
			mode = 2;
		} else if(strmh.chroma == "422") {
			framesize = 2 * strmh.width * strmh.height;
			framesize2 = 2 * cp.w * cp.h;
			ymod = 2;
			mode = 3;
		} else if(strmh.chroma == "422p16") {
			framesize = 4 * strmh.width * strmh.height;
			framesize2 = 4 * cp.w * cp.h;
			ymod = 2;
			mode = 4;
		} else if(strmh.chroma == "444") {
			framesize = 3 * strmh.width * strmh.height;
			framesize2 = 3 * cp.w * cp.h;
			mode = 5;
		} else if(strmh.chroma == "444p16") {
			framesize = 6 * strmh.width * strmh.height;
			framesize2 = 6 * cp.w * cp.h;
			mode = 6;
		} else
			throw std::runtime_error("Unsupported input chroma type '" + strmh.chroma + "'");
		if(cp.x % xmod || cp.y % ymod || cp.w % xmod || cp.h % ymod)
			throw std::runtime_error("Target must be multiple of chroma subsampling'");

		if(cp.x + cp.w < cp.x || cp.x + cp.w > strmh.width || cp.y + cp.h < cp.y ||
			cp.y + cp.h > strmh.height) {
			std::cerr << "crop: Region out of image" << std::endl;
			return 1;
		}


		size_t oheight = strmh.height;
		strmh.width = cp.w;
		strmh.height = cp.h;
		write_or_die(out, static_cast<std::string>(strmh));

		uint64_t curframe = 0;
		std::string _framh, _framh2;
		std::vector<char> buffer;
		std::vector<char> buffer2;
		buffer.resize(framesize);
		buffer2.resize(framesize2);
		struct yuv4mpeg_frame_header framh;

		//Frame loop.
		while(true) {
			unsigned duration = 1;
			if(!read_line2(in, _framh))
				return 0;	//End.
			framh = yuv4mpeg_frame_header(_framh);
			read_or_die(in, &buffer[0], buffer.size());
			do_crop(&buffer2[0], &buffer[0], framesize, oheight, mode, cp);
			write_or_die(out, static_cast<std::string>(framh));
			write_or_die(out, &buffer2[0], buffer2.size());
			curframe += duration;
		}
	} catch(std::exception& e) {
		std::cerr << "crop: Error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
